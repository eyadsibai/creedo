/**
 * 
 */

$(document).ready(

		function() {

			var PARAMETERS = CREEDO.parameters;

			function loadParamsFromServer() {
				var paramArea = this;
				var $this = $(this);
				var componentId = $this.attr('componentId');
				var entryId = $(this).attr('entryId');

				CREEDO.core.requestData(componentId, entryId, function(result) {
					PARAMETERS.renderParameters($this, result);
				});
			}

			function initParamArea() {

				// this points to dom node of parameter area (since called
				// through
				// selector.each)
				var paramArea = this;
				var $paramArea = $(this);

				loadParamsFromServer.apply(this);

				$(this).on(
						'creedo:param-changed',
						function(event, paramName, paramValue) {
							// console.log("param change: " + paramName + "->"
							// + paramValue);

							CREEDO.core.performAction($paramArea
									.attr('componentId'), $paramArea
									.attr('parameterUpdateActionId'), [
									paramName, paramValue ], function() {
								loadParamsFromServer.apply(paramArea);
							});
						});

			}

			$('.parameterArea').each(initParamArea);
		});
