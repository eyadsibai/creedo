/**
 * @author Mario Boley
 * 
 * @since 0.1.2
 * 
 * @version 0.1.2.16
 * 
 */

CREEDO.namespace("CREEDO.dataviewcontainer");

CREEDO.dataviewcontainer = (function() {

	console.log("loading module: CREEDO.dataviewcontainer");

	var creedoCore = CREEDO.core;

	var dataViewContainerId = $("#creedo-dataviewcontainer")
			.attr("componentId");

	var mainTable = $("#content-table .datatable");
	var fixedColumnsTable;
	var metadataTable = $("#content-metadata .datatable");
	var numberOfDataItems;

	var getNumberOfDataItems = function() {
		return numberOfDataItems;
	};

	var processColumnTooltips = function(tooltips) {
		var $columns = $(".datatable-column");
		$.each($columns, function(index, column) {
			$(column).tooltip({
				container : "body",
				placement : "bottom",
				title : tooltips[index],
				trigger : "hover",
				html : "true"
			});
		});
	}

	var processDescription = function(description) {
		$("#datasetNameElement").tooltip({
			container : "body",
			placement : "bottom",
			title : description,
			trigger : "hover",
			html : "true"
		});
	}

	var processTableData = function(data) {
		numberOfDataItems = data.length;
		mainTable.removeClass("hide").dataTable({
			"scrollX" : true,
			"scrollY" : calcTableHeight() + 'px',
			"scrollCollapse" : true,
			"bSortClasses" : false,
			"bLengthChange" : false,
			"iDisplayLength" : 40,
			"bScrollCollapse" : false,
			"fnDrawCallback" : function() {
				if (fixedColumnsTable != null) {
					fixedColumnsTable.fnRedrawLayout();
				}
			}
		});

		fixedColumnsTable = new $.fn.dataTable.FixedColumns(mainTable);

		/*
		 * Here an index is given to each row in the table The index corresponds
		 * to the index of the data received from the server side
		 */
		$.each(data, function(idx, row) {
			mainTable.fnAddData(row, false);
		});
		$("#content-table .loader").addClass("hide");
		mainTable.fnDraw();
		fixedColumnsTable.fnUpdate();
		fixedColumnsTable.fnRedrawLayout();
	};

	// var renderDatatable = function() {
	// // render content(dataset) datatable
	// if (mainTable.length > 0) {
	// console.time('datatable render');
	// creedoCore.requestData(dataViewContainerId, "tableData",
	// processTableData);
	// console.timeEnd('datatable render');
	//
	// }
	// };

	var drawPointCloud = function() {
		var $container = $('#content-point-cloud');
		if ($container.length == 0) {
			return;
		}
		// $container.height($('#content-table').height());
		$container.height(calcTableHeight());

		// Only request point-cloud-data if not already present
		if (window.pointCloudData == null) {
			CREEDO.core.requestData(window.dataViewContainerId, "pointData",
					function(data) {
						window.pointCloudData = data;
						processPointCloudData(data);
					});
		} else {
			processPointCloudData(window.pointCloudData);
		}

		function processPointCloudData(data) {
			var globalSupportSet = getGlobalSupportSet();
			var seriesData = splitSeriesBySupportSet(data, globalSupportSet);

			$container.highcharts({
				chart : {
					type : 'scatter',
					zoomType : 'xy'
				},
				title : {
					text : ''
				},
				xAxis : {
					title : {
						enabled : false
					}
				},
				yAxis : {
					title : {
						enabled : false
					}
				},
				legend : {
					enabled : true
				},

				tooltip : {
					formatter : function() {
						return this.point.objName;
					}
				},
				series : [ {
					turboThreshold : 0,
					name : 'Data',
					color : 'rgba(83, 83, 233, .5)',
					data : seriesData.series1
				}, {
					turboThreshold : 0,
					name : 'Selected',
					color : 'rgba(233, 83, 83, .5)',
					data : seriesData.series2
				} ]
			});
		}

		function splitSeriesBySupportSet(data, globalSupportSet) {
			var series1 = [];
			var series2 = [];

			$.each(data, function(index, item) {
				if ($.inArray(item.id, globalSupportSet) == -1) {
					series1.push(item);
				} else {
					series2.push(item);
				}
			});
			return {
				series1 : series1,
				series2 : series2
			}
		}
	};

	var redrawFilteringDependentViews = function() {
		mainTable.fnDraw();
		drawPointCloud();
	};

	$(document).ready(
			function() {

				console.log("init module: CREEDO.dataviewcontainer");

				// renderDatatable();
				creedoCore.requestData(dataViewContainerId, "tableData",
						processTableData);
				creedoCore.requestData(dataViewContainerId, "columnTooltips",
						processColumnTooltips);
				creedoCore.requestData(dataViewContainerId, "description",
						processDescription);

				$('#tab-content').on('shown.bs.tab', function(e) {
					mainTable.fnDraw();
				});
				$('#tab-point-cloud').on('shown.bs.tab', function(e) {
					drawPointCloud();
				});
				// render metadata table
				$('#tab-metadata').on('shown.bs.tab', function(e) {
					$target = $(e.target);
					if (!$target.data('tableRendered')) {

						metadataTable.dataTable({
							"scrollY" : calcTableHeight() + 'px',
							"scrollCollapse" : true,
							"bSortClasses" : false,
							"bLengthChange" : false,
							"bFilter" : false,
							"paging" : false,
							"bScrollCollapse" : false
						});
						$target.data('tableRendered', true);
					}
					metadataTable.fnDraw();
				});

			});

	return {
		getNumberOfDataItems : getNumberOfDataItems,
		mainTable : mainTable,
		redrawFilteringDependentViews : redrawFilteringDependentViews,
		drawPointCloud : drawPointCloud
	};

}());