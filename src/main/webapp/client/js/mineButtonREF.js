var categoriesSelect = $('#algorithmCategoriesSelect');
var algorithmsArea = $('#refAlgorithmsArea');
var algorithmsSelect = $('#algorithmSelect');
var parametersArea = $('#refParametersArea');
var executeButton = $('#refExecuteButton');
var candidatesContainer = $("#candidate-patterns");
var resultsPatterns = $('#analysis-patterns');
var refBox = $('#refControlBox');
var mineButton = $('#btn-start-mining');
var stopButton = $('#btn-stop-mining');
var isCurrentAlgorithmStoppable = true;

function refMine(e) {
	e.preventDefault();

	/* Reference mining system logic */
	refBox.modal();
}

function getCurrentSelectedCategory() {
	return categoriesSelect.find(":selected").val();
}

function getCategoryAlgorithmsEventHandler() {
	// When category is changed, hide all parameters selected so far
	parametersArea.hide();

	var selected = getCurrentSelectedCategory();
	if (selected == "") {
		algorithmsArea.hide();
		executeButton.addClass('disabled');
	} else {
		CREEDO.miningrunner.getAndShowCategoryAlgorithms(selected);
	}
}

function getCurrentSelectedAlgorithm() {
	return algorithmsSelect.find(":selected").val();
}

/* Parameter selection */
function getAlgorithmParametersEventHandler() {
	var selected = getCurrentSelectedAlgorithm();
	if (selected == "") {
		parametersArea.hide();
		executeButton.addClass('disabled');
	} else {
		CREEDO.miningrunner.selectAlgorithmAndGetParameters(selected,
				updateParameterArea);
	}
}

/**
 * Maps the received parameters to visible control elements. Furthermore it
 * checks for verifiability and validity of the values and conditionally shows
 * hint icons and dis/en-ables the execute button.
 * 
 * @param results
 */
function updateParameterArea(results) {

	var renderParameters = CREEDO.parameters.renderParameters;

	parametersArea.hide();

	var allOk = true;
	executeButton.addClass('disabled');

	var allOk = renderParameters(parametersArea, results);

	// Initialize tooltips
	$("[data-toggle=tooltip]").tooltip();
	$("[data-toggle=popover]").popover();

	$('.creedo-parameter-input').on('creedo:param-changed',
			getCheckParametersHandler);

	parametersArea.show();

	if (allOk) {
		executeButton.removeClass('disabled');
	}
}

function getCheckParametersHandler(event, paramName, paramValue) {
	CREEDO.miningrunner.sendParameterValuesAndGetNewState(paramName,
			paramValue, updateParameterArea);
}

function getExecuteButtonEventHandler(e) {
	var creedoCore = CREEDO.core;
	var miningrunner = CREEDO.miningrunner;

	e.preventDefault();

	candidatesContainer
			.html("<div class=\"text-center\"><img src=\"client/images/loader.gif\" /></div>");

	// Hide box
	refBox.modal('hide');

	// Reset stop-button
	if (isCurrentAlgorithmStoppable) {
		stopButton.removeClass('disabled');
	} else {
		stopButton.addClass('disabled');
	}

	var algorithmName = getCurrentSelectedAlgorithm();

	if (algorithmName != '') {
		// Disable patterns areas
		candidatesContainer.addClass('disabled');
		resultsPatterns.addClass('disabled');

		// Hide "Mine" button and instead show "Stop" button
		mineButton.addClass('hidden');
		stopButton.removeClass('hidden');

		$('#mining-error-notification').addClass('hidden');

		var miningCompleteHandler = function() {
			candidatesContainer.removeClass('disabled');
			resultsPatterns.removeClass('disabled');

			// Hide "Stop" button and show "Mine" button instead
			stopButton.addClass('hidden');
			mineButton.removeClass('hidden');
		};

		creedoCore.performAction(miningrunner.getAnalyticsDashboardServerId(),
				miningrunner.getRunAlgorithmActionId(), {},
				miningrunner.setCandidatePatterns, function(request, status,
						error) {
					miningrunner.showMiningError(request.responseText);
				}, miningCompleteHandler);

	}
}

// function prepareParameterJsonPackage() {
// var params = $('.text-parameter, .range-enumerable-parameter,
// .subset-parameter, .subcollection-parameter');
// var filtered = params.filter(function() {
// var result = $(this).prop('disabled') === undefined
// || $(this).prop('disabled') === false;
// return result;
// });
//
// // Build a data-object containing all parameter value selections by the user
// var data = $();
// var parameters = [];
// $.each(filtered, function(index, current) {
// console.dir(current);
// parameters.push({
// "id" : current.id,
// "value" : current.getParameterValue()
// });
// });
//
// data.parameters = parameters;
//
// return JSON.stringify(data);
// }

$(document).ready(function() {
	var showDialog = $("#btn-start-mining").attr('showDialog') === "true";

	executeButton.addClass('disabled');
	// getCategories();

	// Register event handler for change-events
	categoriesSelect.change(getCategoryAlgorithmsEventHandler);
	algorithmsSelect.change(getAlgorithmParametersEventHandler);

	// Register callback for execute button
	executeButton.click(getExecuteButtonEventHandler);

	if (showDialog) {
		$("#btn-start-mining").click(refMine);
	} else {
		$("#btn-start-mining").click(getExecuteButtonEventHandler);
	}

});
