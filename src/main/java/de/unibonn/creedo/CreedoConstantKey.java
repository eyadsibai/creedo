package de.unibonn.creedo;

/**
 * User: bjacobs Date: 26.05.14 Time: 13:10
 */
public enum CreedoConstantKey {
	LOGIN_INVITATION, // What to display on the main page to encourage user to
						// log in
	ACCOUNT_REQUEST_NOTE, // What to display on account request page
	SURVEY_ADV_TITLE, // How to answer your question title
	SURV_ADV_GENUSR, // related description text
	ATTENTION_INTERRUPTION_OF_ANALYSIS, // warning that user cannot interrupt
										// process
	RATE_ADV_GENUSR, // tell user how rating submitted patterns should be
						// performed
	RATE_OPINION_GENUSR, // tell user how opinion rating should be performed
	OP_INFO_REL, // Opinion about information reliability
	OP_COMPLETE, // Opinion about Information Completeness
	OP_COMPR, // Opinion about Information Comprehensiveness
	OP_INFO_RANK, // Opinion about Information Ranking
	OP_SYS_FRIEND, // User Friendlyness of Pattern Discovery System
	OP_GENERAL, // General Level of end user system satisfaction
	FEEDBACK, // Ask for feedback text
	THANKS, // Text for thanking user to participate
	NEXT_STEP, // Announcement of pattern review survey phase
	REVIEW_ADV, // Advice how to conduct the pattern review in the 2nd survey
				// phase
	REVIEW_TITLE, // Advice how to review the utility of patterns submitted by
					// other participants
	OP_SYS_RANK, // Opinion about System Reliability
	OP_SYS_INVOLV // Opinion about User Involvement
}
