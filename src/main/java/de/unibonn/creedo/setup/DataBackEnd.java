package de.unibonn.creedo.setup;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.springframework.stereotype.Component;

import de.unibonn.creedo.ConfigurationProperties;
import de.unibonn.creedo.common.ContextProvider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

/**
 * Provides access to iBatis sql sessions.
 * 
 * @author Pavel Tokmakov
 * @author Bo Kang
 * @author Bjoern Jacobs
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.3.0
 *
 */
@Component
public class DataBackEnd {

	public static DataBackEnd instance() {
		return (DataBackEnd) ContextProvider.getApplicationContext().getBean("dataBackEnd");
	}

	private Connection getConnection()
			throws ClassNotFoundException, IllegalAccessException, InstantiationException, SQLException {
		String connectionString = "jdbc:mysql://" + config.CREEDO_DB_HOST + ":" + config.CREEDO_DB_PORT + "/";
		Class.forName("com.mysql.jdbc.Driver").newInstance();
		return DriverManager.getConnection(connectionString, config.CREEDO_DB_USER, config.CREEDO_DB_PASS);
	}

	public boolean online() {
		String sql = "SHOW DATABASES LIKE '" + config.CREEDO_DB_SCHEMA + "';";
		try {
			Connection connection = getConnection();
			PreparedStatement statement = connection.prepareStatement(sql);
			ResultSet resultSet = statement.executeQuery();
			boolean result=resultSet.next();
			statement.close();
			resultSet.close();
			connection.close();
			return result;
		} catch (ClassNotFoundException | IllegalAccessException | InstantiationException | SQLException e) {
			return false;
		}
	}

	private SqlSessionFactory sqlSessionFactory;

	private final ConfigurationProperties config;

	public DataBackEnd(ConfigurationProperties config) {
		this.config = config;
	}

	private void initializeSqlSessionFactory() {
		String connectionString = "jdbc:" + config.CREEDO_DB_SYSTEM + "://" + config.CREEDO_DB_HOST + "/"
				+ config.CREEDO_DB_SCHEMA;

		Map<String, String> replacements = new HashMap<>();
		replacements.put("url", connectionString);
		replacements.put("driver", config.CREEDO_DB_DRIVER);
		replacements.put("user", config.CREEDO_DB_USER);
		replacements.put("pass", config.CREEDO_DB_PASS);

		/*
		 * Since MyBatis configuration XML file only allows a static
		 * configuration and no dynamic elements, we have to modify this
		 * configuration before it is used in the builder. The modification
		 * consists of replacing the place-holders in the file with data read
		 * from the config file. (Which the user provided on start-up.
		 */
		try {
			Reader rawResource = Resources.getResourceAsReader("mybatis.config.xml");
			StringReader modifiedResource = new StringReader(replace(rawResource, replacements));

			sqlSessionFactory = new SqlSessionFactoryBuilder().build(modifiedResource);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public SqlSessionFactory getSessionFactory() {
		if (sqlSessionFactory==null) {
			initializeSqlSessionFactory();
		}
		return sqlSessionFactory;
	}

	/**
	 * Reads line-wise from the input reader and replaces all occurrences of
	 * keys provided in the Properties object with the associated values. The
	 * replacement is done on the prefix "${ + key + }".
	 * 
	 * @param input
	 *            Input data
	 * @param replacements
	 *            Replacements map
	 * @return String containing the input data but with replacements applied
	 */
	private static String replace(Reader input, Map<String, String> replacements) {
		BufferedReader reader = new BufferedReader(input);
		StringBuilder builder = new StringBuilder();

		String line;
		try {
			while ((line = reader.readLine()) != null) {
				String newLine = line;
				for (Object object : replacements.keySet()) {
					String key = (String) object;
					newLine = newLine.replace("${" + key + "}", replacements.get(key));
				}
				builder.append(newLine).append("\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return builder.toString();
	}

}