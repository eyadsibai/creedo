package de.unibonn.creedo.setup;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.logging.Logger;

import de.unibonn.creedo.ConfigurationProperties;
import de.unibonn.creedo.utils.SimpleCopyFileVisitor;

public class CreedoInitializer {

	private static final Logger LOGGER = Logger.getLogger(CreedoInitializer.class.getName());

	private final DataBackEnd connectionFactory;

	private final ConfigurationProperties config;

	public CreedoInitializer(ConfigurationProperties properties, DataBackEnd connectionFactory) {
		checkNotNull(properties);
		checkNotNull(connectionFactory);
		this.config = properties;
		this.connectionFactory = connectionFactory;
		initSequence();
	}

	private void initDatabase() {
		String directory = config.CREEDO_ACTIVE_PLUGIN;
		String ressourcePath = "init/" + directory + "/database";
		LOGGER.warning("Init database using ressource paths " + ressourcePath);
		ClassLoader classLoader = getClass().getClassLoader();
		File dir = new File(classLoader.getResource(ressourcePath).getPath());
		DataBackEndInitializer.initialize(dir, config.CREEDO_DB_HOST, config.CREEDO_DB_PORT, config.CREEDO_DB_SCHEMA,
				config.CREEDO_DB_USER, config.CREEDO_DB_PASS, true);

	}

	private void initContentDir() throws RuntimeException {
		LOGGER.warning("Copy files to content folder " + ServerPaths.ABS_PATH_TO_CONFIGURED_RESSOURCE_DIR
				+ " from ressource path " + ServerPaths.ABS_PATH_TO_INIT_CONTENTFILES + " (overwriting existing)");
		try {
			Files.walkFileTree(ServerPaths.ABS_PATH_TO_INIT_CONTENTFILES,
					new SimpleCopyFileVisitor(ServerPaths.ABS_PATH_TO_CONFIGURED_RESSOURCE_DIR,
							StandardCopyOption.REPLACE_EXISTING, StandardCopyOption.COPY_ATTRIBUTES));
		} catch (IOException e) {
			throw new RuntimeException("Could not access content directory");
		}
	}

	private void initSequence() {
		LOGGER.info("Checking database backeend...");
		if (connectionFactory.online()) {
			LOGGER.info("Schema exists.");
		} else {
			LOGGER.severe("Schema does not exist or cannot be accessed.");
			initDatabase();
			initContentDir();
		}
		LOGGER.info("Creedo is up and running...");
	}

}
