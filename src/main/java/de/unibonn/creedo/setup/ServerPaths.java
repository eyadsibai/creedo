package de.unibonn.creedo.setup;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.ConfigurationProperties;

/**
 * Contains static members that allow to resolve pathes on the creedo server.
 * 
 * WARNING: this might fail if jvm does not perform lazy inits of static fields,
 * because it relies on configuration properties already loaded by spring.
 * Should be turned into object similar to other components.
 * 
 * @author Bjoern Jacobs
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.3.0
 *
 */
public class ServerPaths {

	private static final ClassLoader CLASS_LOADER = ServerPaths.class
			.getClassLoader();

	private static final String INIT_DB_SUBDIR = "/database";

	private static final String INIT_CONTENTFILES_SUBDIR = "/contentfiles";

	private static final String INIT_BASE_PATH = "init/";

	public static final Path ABS_PATH_TO_INIT_CONTENTFILES;
	static {
		try {
			ABS_PATH_TO_INIT_CONTENTFILES = Paths
					.get(CLASS_LOADER
							.getResource(
									INIT_BASE_PATH
											+ ConfigurationProperties.get().CREEDO_ACTIVE_PLUGIN
											+ INIT_CONTENTFILES_SUBDIR).toURI());
		} catch (URISyntaxException e) {
			throw new RuntimeException(
					"Could not find path to content init files");
		}
	}

	/*
	 * This should also be turned to new io Path. WARNING when doing so, must
	 * use URI-based converstion to work under windows
	 */
	public static final File ABS_PATH_TO_INIT_DBSCRIPTS = new File(
			CLASS_LOADER
					.getResource(
							INIT_BASE_PATH
									+ ConfigurationProperties.get().CREEDO_ACTIVE_PLUGIN
									+ INIT_DB_SUBDIR).getPath());

	public static final Path ABS_PATH_TO_CONFIGURED_RESSOURCE_DIR = FileSystems
			.getDefault().getPath(ConfigurationProperties.get().USER_FOLDER);

	public static String getContentFileContentAsString(String pageFileName) {

		try {
			InputStream resourceAsStream = Files
					.newInputStream(ApplicationRepositories.CONTENT_FOLDER_REPOSITORY
							.getEntry(pageFileName).getContent());
			Scanner scanner = new Scanner(resourceAsStream, "UTF-8");
			String pageContent = scanner.useDelimiter("\\A").next();
			scanner.close();
			return pageContent;
		} catch (IOException e) {
			throw new IllegalStateException("could not access ressource file: "
					+ pageFileName, e);
		}
	}

}
