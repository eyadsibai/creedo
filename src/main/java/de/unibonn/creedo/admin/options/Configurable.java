package de.unibonn.creedo.admin.options;

import de.unibonn.realkd.common.parameter.ParameterContainer;

public interface Configurable<OptionsClass extends ParameterContainer> {

	public OptionsClass getDefaultOptions();
	
	@SuppressWarnings("unchecked")
	public default OptionsClass getOptions() {
		String key=this.getOptionsSerializationId();
		if (!Options.REPOSITORY.getAllIds().contains(key)) {
			Options.REPOSITORY.add(key, getDefaultOptions());
		}

		return (OptionsClass) Options.REPOSITORY.get(key);
	}
	
	public default String getOptionsSerializationId() {
		return this.getClass().getSimpleName();
	}

}
