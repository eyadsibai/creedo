package de.unibonn.creedo.admin.users;

import java.util.Set;

public interface UserDetails {

	public Set<UserGroup> groups();

	public String hashedPassword();
	
	public default boolean active() {
		return true;
	}

}