package de.unibonn.creedo.admin.users;

import java.util.Set;

import com.google.common.collect.ImmutableSet;

import de.unibonn.creedo.common.BCrypt;

public class ExampleUserDetails implements UserDetails {

	private static final String CLEAR_PASSWORD = "creedo";
	
	private static final String HASHED_PASSWORD= BCrypt.hashpw(CLEAR_PASSWORD, BCrypt.gensalt(12));

	private static final ImmutableSet<UserGroup> GROUPS = ImmutableSet.of(DefaultUserGroup.EXAMPLE);

	public ExampleUserDetails() {
		;
	}

	@Override
	public Set<UserGroup> groups() {
		return GROUPS;
	}

	@Override
	public String hashedPassword() {
		return HASHED_PASSWORD;
	}

}
