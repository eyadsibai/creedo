package de.unibonn.creedo.admin.users;

public enum DefaultUserGroup implements UserGroup {

	ANONYMOUS,

	/**
	 * Members can edit repositories.
	 */
	ADMINISTRATOR,

	/**
	 * Members should see introspection elements and debug functionality where
	 * available.
	 */
	DEVELOPER,

	/**
	 * Members of this group can be created as part of demo content.
	 */
	EXAMPLE,

	/**
	 * Members have gone through a registration process; their id is a valid
	 * e-mail address
	 */
	REGISTERED;

}
