package de.unibonn.creedo.admin.users;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.google.common.collect.ImmutableSet;

import de.unibonn.creedo.ConfigurationProperties;
import de.unibonn.creedo.common.BCrypt;
import de.unibonn.creedo.repositories.Repository;
import de.unibonn.creedo.repositories.mybatisimpl.MyBatisRepository;

public class Users {

	private static final String ROOT_USER_ID = "root";

	private static final Repository<String, UserDetails> REPOSITORY = new MyBatisRepository<>("Users",
			"admin__users", "admin__users_parameters");

	static {
		if (!REPOSITORY.getOpt(ROOT_USER_ID).isPresent()) {
			UserDetails rootDetails = new RootUserDetails(ConfigurationProperties.get().DEFAULT_ROOT_PASSWORD);
			REPOSITORY.add(ROOT_USER_ID, rootDetails);
		}
	}

	public static final CreedoUser DEFAULT_USER = new CreedoUser() {

		@Override
		public String id() {
			return "default";
		}

		@Override
		public Set<UserGroup> groups() {
			return ImmutableSet.of(DefaultUserGroup.ANONYMOUS);
		}

		@Override
		public String hashedPassword() {
			return BCrypt.hashpw("", BCrypt.gensalt(12));
		}

		@Override
		public boolean active() {
			return true;
		}
	};

	private static class CreedoUserImpl implements CreedoUser {

		private final String id;

		private final boolean active;
		
		private final Set<UserGroup> groups;

		private final String hashedPassword;

		public CreedoUserImpl(String id, UserDetails details) {
			this.id = id;
			this.groups = details.groups();
			this.hashedPassword = details.hashedPassword();
			this.active = details.active();
		}

		@Override
		public String id() {
			return id;
		}

		@Override
		public Set<UserGroup> groups() {
			return groups;
		}

		@Override
		public String hashedPassword() {
			return hashedPassword;
		}

		@Override
		public boolean active() {
			return active;
		}

	}

	public static Repository<String, UserDetails> detailsRepository() {
		return REPOSITORY;
	}

	public static Optional<CreedoUser> getUser(String id) {
		Optional<UserDetails> detailsOption = REPOSITORY.getOpt(id);
		return detailsOption.flatMap(details -> Optional.of(new CreedoUserImpl(id, details)));
	}
	
	public static void add(String id, UserDetails details) {
		REPOSITORY.add(id, details);
	}

	public static List<String> ids() {
		return REPOSITORY.getAllIds();
	}

}
