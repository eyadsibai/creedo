package de.unibonn.creedo.admin.users;

import static de.unibonn.realkd.common.parameter.DefaultSubCollectionParameter.getDefaultSubSetParameter;

import java.util.List;
import java.util.Set;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import de.unibonn.creedo.common.parameters.SecureParameter;
import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.DefaultSubCollectionParameter.CollectionComputer;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;

public class DefaultUserDetails implements ParameterContainer, UserDetails {

	private static final ImmutableList<Boolean> BOOLS = ImmutableList.of(Boolean.FALSE, Boolean.TRUE);

	private final SecureParameter<String> password;

	private final SubCollectionParameter<UserGroup, Set<UserGroup>> optionalGroups;

	private final Parameter<Boolean> active;

	public DefaultUserDetails() {
		this.password = new PasswordParameter();
		this.optionalGroups = getDefaultSubSetParameter("Groups",
				"The optional groups this user belongs to (user will always be member of REGISTERED).",
				new CollectionComputer<Set<UserGroup>>() {
					@Override
					public Set<UserGroup> computeCollection() {
						return UserGroups.get().optional();
					}
				});
		this.active = new DefaultRangeEnumerableParameter<Boolean>("Active",
				"Activation status of account (inactive users cannot log in)", Boolean.class, () -> BOOLS);
	}

	public DefaultUserDetails(String rawPassword, Set<UserGroup> userGroups) {
		this();
		password.setByClearValue(rawPassword);
		optionalGroups.set(userGroups);
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return ImmutableList.of(active, password, optionalGroups);
	}

	@Override
	public Set<UserGroup> groups() {
		return Sets.union(ImmutableSet.of(DefaultUserGroup.REGISTERED), optionalGroups.getCurrentValue());
	}

	@Override
	public String hashedPassword() {
		return password.getCurrentValue();
	}

	@Override
	public boolean active() {
		return active.getCurrentValue();
	}

}
