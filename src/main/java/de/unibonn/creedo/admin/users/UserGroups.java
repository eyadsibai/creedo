package de.unibonn.creedo.admin.users;

import java.util.Set;

import com.google.common.collect.ImmutableSet;

/**
 * Singleton that defines certain collections of user groups.
 * 
 * @author Mario Boley
 * 
 * @since 0.3.0
 * 
 * @version 0.3.0
 *
 */
public class UserGroups {

	private static final UserGroups INSTANCE=new UserGroups();
	
	private UserGroups() {
		;
	}
	
	public static UserGroups get() {
		return INSTANCE;
	}
	
	public Set<UserGroup> all() {
		return ImmutableSet.copyOf(DefaultUserGroup.values());
	}

	/**
	 * Collection of user groups that ordinary users can be part of.
	 */
	public Set<UserGroup> optional() {
		return ImmutableSet.of(DefaultUserGroup.ADMINISTRATOR, DefaultUserGroup.DEVELOPER);
	}

}
