package de.unibonn.creedo.studies;

/**
 * Encodes the specific roles that a user can have during a study.
 * 
 * @author Mario Boley
 * 
 * @since 0.1.2
 * 
 * @version 0.1.2.1
 *
 */
public enum StudyUserRole {

	/**
	 * NOTE: Not used at the moment. Planned future use is to signify the right
	 * to see study results and switch study phases (plus potential special
	 * assignments).
	 */
	OWNER,

	/**
	 * Users that work on study tasks.
	 */
	PARTICIPANT,

	/**
	 * Users that evaluate the results of study tasks.
	 */
	EVALUATOR;

}
