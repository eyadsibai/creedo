package de.unibonn.creedo.studies;

import static com.google.common.base.Preconditions.checkState;
import static de.unibonn.realkd.common.parameter.Parameters.stringParameter;

import java.nio.file.Path;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.admin.users.Users;
import de.unibonn.creedo.repositories.IdentifierInRepositoryParameter;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.creedo.studies.assignmentschemes.AssignmentPhase;
import de.unibonn.creedo.studies.designs.StudyDesign;
import de.unibonn.realkd.common.parameter.DefaultParameter;
import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter.RangeComputer;
import de.unibonn.realkd.common.parameter.DefaultSubCollectionParameter;
import de.unibonn.realkd.common.parameter.DefaultSubCollectionParameter.CollectionComputer;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.RangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.ValueValidator;

public class StudyBuilder implements ParameterContainer {

	private final Parameter<String> nameParameter;

	private final Parameter<String> descriptionParameter;

	private final Parameter<String> imgUrlParameter;

	private final Parameter<String> imgCreditsParameter;

	private final RangeEnumerableParameter<String> studyDesignParameter;

	private final RangeEnumerableParameter<String> statusParameter;

	private final DefaultSubCollectionParameter<String, Set<String>> participantsParamter;

	private final DefaultSubCollectionParameter<String, Set<String>> evaluatorsParamter;

	public StudyBuilder() {
		nameParameter = stringParameter("Name", "The name of the study.", "",
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");

		descriptionParameter = stringParameter("Description",
				"A descripton that explains the study to the study users.", "",
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");

		imgUrlParameter = new IdentifierInRepositoryParameter<Path>(
				"ImgUrl",
				"File with the image shown in study description.",
				ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
				Repositories
						.getIdIsFilenameWithExtensionPredicate(Repositories.IMAGE_FILE_EXTENSIONS));

		imgCreditsParameter = new DefaultParameter<String>("ImgCredits",
				"The credits of the image used in study description.",
				String.class, "", input -> input,
				ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");

		studyDesignParameter = new DefaultRangeEnumerableParameter<String>(
				"Design", "The design for this study", String.class,
				new RangeComputer<String>() {
					@Override
					public List<String> computeRange() {
						return ApplicationRepositories.STUDY_DESIGN_REPOSITORY
								.getAllIds();
					}
				});

		// statusParameter = new DefaultRangeEnumerableParameter<StudyState>(
		// "State", "The status of this study", StudyState.class,
		// new RangeComputer<StudyState>() {
		// @Override
		// public List<StudyState> computeRange() {
		// return Arrays.asList(StudyState.values());
		// }
		// });

		statusParameter = new DefaultRangeEnumerableParameter<String>("State",
				"The current assignment phase of this study.",
				AssignmentPhase.class, new RangeComputer<String>() {

					/**
					 * TODO: This is a shortcut for the IDEA demo to cut down on
					 * long loading times. In the future there will be no study
					 * builder but study states will be managed via a specific
					 * study console.
					 */
					private List<String> options = ImmutableList.of("SETUP",
							"TRIAL", "EVALUATION", "CONCLUSION");

					@Override
					public List<String> computeRange() {
						return options;
						// return
						// ApplicationRepositories.STUDY_DESIGN_REPOSITORY
						// .getEntry(
						// studyDesignParameter.getCurrentValue())
						// .getContent().build().getAssignmentScheme()
						// .getAssignmentPhases().stream()
						// .map(x -> x.toString())
						// .collect(Collectors.toList());
					}
				}, studyDesignParameter);

		participantsParamter = DefaultSubCollectionParameter
				.getDefaultSubSetParameter("Participants",
						"Set of users that work on study analysis tasks.",
						new CollectionComputer<Set<String>>() {
							@Override
							public Set<String> computeCollection() {
								return Sets.newHashSet(Users.ids());
							}
						});
		evaluatorsParamter = DefaultSubCollectionParameter
				.getDefaultSubSetParameter("Evaluators",
						"Set of users that evaluate the study results.",
						new CollectionComputer<Set<String>>() {
							@Override
							public Set<String> computeCollection() {
								return Sets.newHashSet(Users.ids());
							}
						});

		// dynamicRoleToUserParameters = new HashMap<>();

		// defaultParameterContainer = new DefaultParameterContainer();
		//
		// defaultParameterContainer.addAllParameters(Arrays.asList(
		// (Parameter<?>) nameParameter, descriptionParameter,
		// imgUrlParameter, imgCreditsParameter, studyDesignParameter,
		// statusParameter, participantsParamter, evaluatorsParamter));
	}

	//
	// private void bufferPhaseOptions() {
	// if (studyDesignParameter.isValid()) {
	// // StudyDesign design =
	// // ApplicationRepositories.STUDY_DESIGN_REPOSITORY
	// // .getEntry(studyDesignParameter.getCurrentValue())
	// // .getContent().build();
	// // phaseOptions = design.getAssignmentScheme().getAssignmentPhases()
	// // .stream().map(x -> x.toString())
	// // .collect(Collectors.toList());
	// AssignmentScheme scheme = (AssignmentScheme)
	// ApplicationRepositories.STUDY_DESIGN_REPOSITORY
	// .getEntry(studyDesignParameter.getCurrentValue())
	// .getContent().findParameterByName("Assignment Scheme")
	// .getCurrentValue();
	// phaseOptions = scheme.getAssignmentPhases().stream()
	// .map(x -> x.toString()).collect(Collectors.toList());
	// }
	// }

	public Study build() {
		validate();
		// Finding and building the design takes 3 seconds on my machine
		// (Mario)!!!
		StudyDesign design = ApplicationRepositories.STUDY_DESIGN_REPOSITORY
				.getEntry(studyDesignParameter.getCurrentValue()).getContent()
				.build();
		Optional<AssignmentPhase> optionalPhase = design
				.getAssignmentScheme()
				.getAssignmentPhases()
				.stream()
				.filter(x -> x.toString().equals(
						statusParameter.getCurrentValue())).findFirst();

		checkState(optionalPhase.isPresent(),
				"Assignment scheme does not provide selected phase: "
						+ statusParameter.getCurrentValue());
		return new Study(nameParameter.getCurrentValue(),
				descriptionParameter.getCurrentValue(),
				imgUrlParameter.getCurrentValue(),
				imgCreditsParameter.getCurrentValue(), design,
				optionalPhase.get(), participantsParamter.getCurrentValue(),
				evaluatorsParamter.getCurrentValue());
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		// return defaultParameterContainer.getTopLevelParameters();
		return ImmutableList.of(nameParameter, descriptionParameter,
				imgUrlParameter, imgCreditsParameter, studyDesignParameter,
				statusParameter, participantsParamter, evaluatorsParamter);
	}

}
