package de.unibonn.creedo.studies.ui;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.studies.RatingDAO;
import de.unibonn.creedo.studies.assignmentschemes.EvaluationAssignment;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.ui.core.UiRegister.FrameCloser;
import de.unibonn.creedo.ui.core.UiRegister.IdGenerator;
import de.unibonn.creedo.webapp.dashboard.study.RatingSystem;
import de.unibonn.creedo.webapp.studies.rating.RatingMetric;
import de.unibonn.creedo.webapp.studies.rating.ResultRating;

public class EvaluationSessionTracker implements UiComponent {

	private class SubmitRatingsAction implements Action {

		private final int id;

		private final FrameCloser closer;

		public SubmitRatingsAction(int id, FrameCloser closer) {
			this.id = id;
			this.closer = closer;
		}

		@Override
		public String getReferenceName() {
			return "Submit ratings";
		}

		@Override
		public ResponseEntity<String> activate(String... params) {
			if (!allRatingsCollected()) {
				return new ResponseEntity<String>(
						"Please rate all results according to all metrics",
						HttpStatus.FORBIDDEN);
			}
			submitRatings();
			closer.requestClose();
			return new ResponseEntity<String>(HttpStatus.OK);
		}

		@Override
		public ClientWindowEffect getEffect() {
			return ClientWindowEffect.CLOSE;
		}

		@Override
		public int getId() {
			return id;
		}

	}

	private final int id;
	private final Model model;
	private final RatingSystem ratingSystem;
	private final EvaluationAssignment assignment;
	private final SubmitRatingsAction submitAction;

	public EvaluationSessionTracker(IdGenerator idGenerator,
			RatingSystem ratingSystem, EvaluationAssignment assignment,
			FrameCloser closer) {
		this.id = idGenerator.getNextId();
		this.ratingSystem = ratingSystem;
		this.assignment = assignment;
		this.model = new BindingAwareModelMap();
		this.submitAction = new SubmitRatingsAction(idGenerator.getNextId(),
				closer);
	}

	private void submitRatings() {
		RatingDAO.INSTANCE.saveRating(assignment.getStudyName(),
				getResultRatings());
	}

	private List<ResultRating> getResultRatings() {
		List<ResultRating> resultRatings = new ArrayList<>();
		String userId = assignment.getUser().id();
		for (Integer resultId : assignment.getResultIds()) {
			List<RatingMetric> metrics = ratingSystem.getMetrics();
			List<Integer> ratings = ratingSystem.getPatternRatings(resultId);
			for (int i = 0; i < ratings.size(); i++) {
				resultRatings.add(new ResultRating(resultId, userId, metrics
						.get(i), metrics.get(i).getRatingOptions()[ratings
						.get(i)].getValue()));
			}
		}
		return resultRatings;
	}

	private boolean allRatingsCollected() {
		for (Integer resultId : assignment.getResultIds()) {
			List<Integer> ratings = ratingSystem.getPatternRatings(resultId);
			for (int i = 0; i < ratings.size(); i++) {
				if (ratings.get(i) == null) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getView() {
		return "../static/emptyPage.jsp";
	}

	@Override
	public Model getModel() {
		return model;
	}

	@Override
	public List<Action> getExternalActions() {
		return ImmutableList.of(submitAction);
	}

}
