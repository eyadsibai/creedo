package de.unibonn.creedo.studies;

import java.util.List;

import org.apache.ibatis.exceptions.PersistenceException;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import de.unibonn.creedo.setup.DataBackEnd;
import de.unibonn.creedo.webapp.studies.rating.ResultRating;

/**
 * @author bkang
 */

public class RatingDAO {

	public static final RatingDAO INSTANCE = new RatingDAO();
	private final SqlSessionFactory sqlSessionFactory;

	private RatingDAO() {
		this.sqlSessionFactory = DataBackEnd.instance().getSessionFactory();
	}

	public List<Integer> getRatingIdsGivenByUserInStudy(String userId,
			String studyName) {
		SqlSession sqlSession = sqlSessionFactory.openSession();
		RatingMapper ratingMapper = sqlSession.getMapper(RatingMapper.class);
		try {
			return ratingMapper.getRatingIdsGivenByUserInStudy(userId,
					studyName);
		} catch (PersistenceException pe) {
			pe.printStackTrace();
			return null;
		} finally {
			sqlSession.commit();
			sqlSession.close();
		}
	}

	public void saveRating(String studyName, List<ResultRating> resultRatings) {
		SqlSession session = sqlSessionFactory.openSession();
		RatingMapper ratingMapper = session.getMapper(RatingMapper.class);
		try {
			for (ResultRating resultRating : resultRatings) {
				ratingMapper.saveRating(resultRating.userId, studyName,
						resultRating.resultId, resultRating.metric.getName(),
						resultRating.value);
			}
		} catch (PersistenceException pe) {
			pe.printStackTrace();
		} finally {
			session.commit();
			session.close();
		}
	}
}
