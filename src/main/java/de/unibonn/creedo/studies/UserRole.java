package de.unibonn.creedo.studies;

public enum UserRole {
	PARTICIPANT {
		@Override
		public String getName() {
			return "Study Participant";
		}
	},
	EVALUATOR {
		@Override
		public String getName() {
			return "Study Evaluator";
		}
	},
	OWNER {
		@Override
		public String getName() {
			return "Study Owner";
		}
	};
	
	public abstract String getName();
	
}
