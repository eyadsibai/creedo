package de.unibonn.creedo.studies.assignmentschemes;

import java.util.List;
import java.util.function.Supplier;

import de.unibonn.creedo.admin.users.CreedoUser;
import de.unibonn.creedo.studies.designs.TaskSpecification;

/**
 * Represents the assignment to evaluate a set of results of a given task.
 * 
 * @author Mario Boley
 * 
 * @since 0.1.2
 * 
 * @version 0.1.2.1
 *
 */
public final class EvaluationAssignment extends Assignment {

	private final List<Integer> resultIds;

	public EvaluationAssignment(String studyName, CreedoUser user,
			TaskSpecification task, List<Integer> resultIds,
			Supplier<Boolean> isValid) {
		super(studyName, user, task, isValid);
		this.resultIds = resultIds;
	}

	public List<Integer> getResultIds() {
		return resultIds;
	}

}
