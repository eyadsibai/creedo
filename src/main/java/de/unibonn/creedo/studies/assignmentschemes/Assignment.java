package de.unibonn.creedo.studies.assignmentschemes;

import java.util.function.Supplier;

import de.unibonn.creedo.admin.users.CreedoUser;
import de.unibonn.creedo.studies.designs.TaskSpecification;

/**
 * Data class capturing study assignments.
 * 
 * @author Mario Boley
 * 
 * @since 0.1.2
 * 
 * @version 0.1.2.1
 *
 */
public abstract class Assignment {

	private final String studyName;

	private final TaskSpecification task;

	private final CreedoUser user;

	private final Supplier<Boolean> isValid;

	public Assignment(String studyName, CreedoUser user, TaskSpecification task,
			Supplier<Boolean> isValid) {
		this.studyName = studyName;
		this.task = task;
		this.user = user;
		this.isValid = isValid;
	}

	public final String getStudyName() {
		return studyName;
	}

	public final TaskSpecification getTask() {
		return task;
	}

	public final CreedoUser getUser() {
		return user;
	}

	public final boolean isValid() {
		return isValid.get();
	}

}
