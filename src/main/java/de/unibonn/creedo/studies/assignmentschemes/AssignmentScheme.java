package de.unibonn.creedo.studies.assignmentschemes;

import com.google.common.collect.ImmutableList;

public interface AssignmentScheme {

//	public abstract List<DashboardLink> getTrialAssignments(
//			List<SystemSpecification> systemSpecs,
//			List<TaskSpecification> taskSpecs, String studyName,
//			String studyDescr, String imgURL, String imgCredits, StudyState state, User user);
//
//	public abstract List<DashboardLink> getEvaluationAssignments(
//			List<TaskSpecification> taskSpecs, String studyName,
//			String studyDescr, String imgURL, String imgCredits, StudyState state, User user);

//	public abstract String getName();
	
	public abstract ImmutableList<AssignmentPhase> getAssignmentPhases();

}