package de.unibonn.creedo.studies.state.mybatisimpl;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import de.unibonn.creedo.studies.state.TrialSession;
import de.unibonn.creedo.webapp.studies.ResultDataContainer;

public interface OldCreedoStudyStateMapper {

	public List<Integer> getAllResultIds();

	public void addResult(@Param("result_id") int resultId,
			@Param("session_id") int sessionId,
			@Param("result_builder_content") String resultBuilderContent,
			@Param("result_seconds_until_saved") int secondsUntilSaved);

	public void addResultWithAutoId(@Param("session_id") int sessionId,
			@Param("result_builder_content") String resultBuilderContent,
			@Param("result_seconds_until_saved") int secondsUntilSaved);

	public ResultDataContainer getResult(@Param("id") int id);

	public List<ResultDataContainer> getResultsWhere(
			@Param("study_name") String studyName,
			@Param("condition") String condition);

	public List<Integer> getAllTrialSessionIds();

	public List<MyBatisTrialSessionQueryResult> getTrialSessionsWhere(
			@Param("study_name") String studyName,
			@Param("condition") String condition);

	public void addTrialSession(@Param("session_id") int sessionId,
			@Param("session") TrialSession session);

	public void addTrialSessionWithAutoId(@Param("session") TrialSession session);

	public Integer getLastInsertedId();

}
