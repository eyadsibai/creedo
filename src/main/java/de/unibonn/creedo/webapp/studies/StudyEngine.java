package de.unibonn.creedo.webapp.studies;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.admin.users.CreedoUser;
import de.unibonn.creedo.repositories.RepositoryEntry;
import de.unibonn.creedo.studies.Study;
import de.unibonn.creedo.studies.StudyBuilder;
import de.unibonn.creedo.ui.indexpage.DashboardLink;
import de.unibonn.creedo.ui.indexpage.DashboardLinkProvider;

/**
 * Builds all study-related dashboard links for a user.
 *
 * @author Mario Boley
 * @author Björn Jacobs
 * 
 * @since 0.1.0
 * 
 * @version 0.1.2.1
 * 
 */
public class StudyEngine implements DashboardLinkProvider {

	private static final Logger LOGGER = Logger.getLogger(StudyEngine.class
			.getName());

	public static final StudyEngine INSTANCE = new StudyEngine();

	private StudyEngine() {
		;
	}

	public List<DashboardLink> getDashboardLinks(CreedoUser user) {
		LOGGER.log(Level.FINE, "Start creating study assignment links");
		List<DashboardLink> results = new ArrayList<>();
		for (RepositoryEntry<String, StudyBuilder> studyEntry : ApplicationRepositories.STUDY_REPOSITORY
				.getAllEntries()) {
			LOGGER.fine(() -> "Start materializing study '"
					+ studyEntry.getId() + "'");
			Study study = studyEntry.getContent().build();
			LOGGER.fine(() -> "Done materializing study '" + studyEntry.getId()
					+ "'");
			results.addAll(study.getDashboardLinks(user));
		}
		LOGGER.fine("Done creating study assignment links");
		return results;
	}

	@Override
	public String getTitle() {
		return "Study Assignments";
	}
}
