package de.unibonn.creedo.webapp.studies.rating;

/**
 * Represents a rating score of a certain result given according to a certain
 * metric by a certain user.
 * 
 * @author Bo Kang
 * 
 * @since 0.1.0
 * 
 * @version 0.1.0.1
 * 
 */
public class ResultRating {

	final public int resultId;

	final public String userId;

	final public RatingMetric metric;

	final public double value;

	public ResultRating(int resultId, String userId, RatingMetric metric,
			double value) {
		this.resultId = resultId;
		this.userId = userId;
		this.metric = metric;
		this.value = value;
	}

}
