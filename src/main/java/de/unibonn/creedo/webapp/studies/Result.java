package de.unibonn.creedo.webapp.studies;

import de.unibonn.realkd.patterns.PatternBuilder;

/**
 * <p>
 * Represents a single trial result that has been produced by some study
 * participant.
 * </p>
 * <p>
 * NOTE: The id corresponds to the id of the result in the db table for results.
 * At the same time this id is used for the web pattern on the analytics
 * dashboard. I assume that this is also important to associate the ratings back
 * to the right result later. This is a very fragile system with a lot of
 * cross-layer dependencies.
 * </p>
 * 
 * @author Björn Jacobs
 * 
 * @since 0.1.0
 * 
 * @version 0.1.2.1
 *
 */
public class Result {

	private final PatternBuilder patternBuilder;

	private final int sessionId;

	private final int secondsInSessionUntilSaved;

	public Result(PatternBuilder patternBuilder, int sessionId,
			int secondsInSessionUntilSaved) {
		this.patternBuilder = patternBuilder;
		this.sessionId = sessionId;
		this.secondsInSessionUntilSaved = secondsInSessionUntilSaved;
	}

	public PatternBuilder getPatternBuilder() {
		return patternBuilder;
	}

	public int getSessionId() {
		return sessionId;
	}

	public int getSecondsInSessionUntilSaved() {
		return secondsInSessionUntilSaved;
	}

	public String toString() {
		return "Result " + patternBuilder + " found in session " + sessionId;
	}

}
