package de.unibonn.creedo.webapp.dashboard.mining;

import com.google.common.collect.Sets;

import de.unibonn.creedo.ui.core.UiRegister.IdGenerator;
import de.unibonn.creedo.webapp.dashboard.mining.rankerfactories.DefaultRankerFactory;
import de.unibonn.creedo.webapp.dashboard.mining.rankerfactories.KnowledgeModelRankerFactory;
import de.unibonn.creedo.webapp.dashboard.mining.rankerfactories.RankerFactory;
import de.unibonn.creedo.webapp.viewmodels.DeveloperViewModel;
import de.unibonn.realkd.algorithms.AlgorithmFactory;
import de.unibonn.realkd.algorithms.MiningAlgorithmFactory;
import de.unibonn.realkd.common.parameter.*;
import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter.RangeComputer;
import de.unibonn.realkd.common.parameter.DefaultSubCollectionParameter.CollectionComputer;
import de.unibonn.realkd.data.DataWorkspace;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Abstract class for builders of MiningSystems that creates MiningAlgorithms
 * and passes them to concrete subclasses via the concretelyBuild-hook.
 * 
 * @author Björn Jacobs
 * 
 * @since 0.1.0
 * 
 * @version 0.1.1.1
 * 
 */
public abstract class MiningSystemBuilder<T> implements ParameterContainer {

	private static List<RankerFactory> rankerFactoryOptions = Arrays.asList(
			new DefaultRankerFactory(), new KnowledgeModelRankerFactory());

	protected final Parameter<Set<MiningAlgorithmFactory>> algorithms;

	private final Parameter<RankerFactory> postProcessor;

	protected final DefaultParameterContainer parameterContainer;

	public MiningSystemBuilder() {
		// this.algorithms = new AlgorithmParameter();
		this.algorithms = DefaultSubCollectionParameter
				.getDefaultSubSetParameter(
						"Algorithms",
						"The algorithms that will be available in the mining system",
						new CollectionComputer<Set<MiningAlgorithmFactory>>() {
							@Override
							public Set<MiningAlgorithmFactory> computeCollection() {
								return Sets.newHashSet(AlgorithmFactory
										.values());
							}

						});
		this.parameterContainer = new DefaultParameterContainer();
		this.postProcessor = new DefaultRangeEnumerableParameter<RankerFactory>(
				"Post processor",
				"A post-processor which is applied to the output of any mining algorithm started with the system.",
				RankerFactory.class, new RangeComputer<RankerFactory>() {
					@Override
					public List<RankerFactory> computeRange() {
						return rankerFactoryOptions;
					}
				});

		this.parameterContainer.addAllParameters(Arrays.asList(algorithms,
				postProcessor));
	}

	public final MiningSystem build(IdGenerator idGenerator,
			DeveloperViewModel developerViewModel, DataWorkspace dataWorkspace) {
		validate();
		fixRankerFactory();
		return concreteBuild(idGenerator, developerViewModel, dataWorkspace);
	}

	protected abstract MiningSystem concreteBuild(IdGenerator idGenerator,
			DeveloperViewModel developerViewModel, DataWorkspace dataWorkspace);

	public T setRankerFactory(RankerFactory rankerFactory) {
		this.postProcessor.set(rankerFactory);
		return (T) this;
	}

	public RankerFactory getRankerFactory() {
		return this.postProcessor.getCurrentValue();
	}

	private void fixRankerFactory() {
		if (this.postProcessor.getCurrentValue() == null) {
			this.setRankerFactory(new DefaultRankerFactory());
		}
	}

	public T setAlgorithmFactories(
			Collection<MiningAlgorithmFactory> algorithmFactories) {
		this.algorithms.set(Sets.newHashSet(algorithmFactories));
		return (T) this;
	}

	public Collection<MiningAlgorithmFactory> getAlgorithmFactories() {
		return algorithms.getCurrentValue();
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return this.parameterContainer.getTopLevelParameters();
	}

}
