package de.unibonn.creedo.webapp.dashboard.mining;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Logger;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import com.google.common.collect.ImmutableSet;

import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.ActionProvider;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.ui.core.UiRegister.IdGenerator;
import de.unibonn.creedo.webapp.dashboard.mining.rankers.Ranker;
import de.unibonn.realkd.algorithms.MiningAlgorithm;
import de.unibonn.realkd.algorithms.StoppableMiningAlgorithm;
import de.unibonn.realkd.discoveryprocess.DiscoveryProcess;
import de.unibonn.realkd.patterns.Pattern;

/**
 * UI component that corresponds to a visual pattern mining launcher on an
 * analytics dashboard.
 * 
 * @author Björn Jacobs
 * @author Mario Boley
 * @author Bo Kang
 * @author Pavel Tokmakov
 * 
 * @since 0.1.0
 * 
 * @version 0.1.1.1
 * 
 */
public class MiningSystem implements UiComponent, ActionProvider {

	private static final Logger LOGGER = Logger.getLogger(MiningSystem.class
			.getName());

	private class StopMiningAlgorithmAction implements Action {

		private final int id;

		public StopMiningAlgorithmAction(int id) {
			this.id = id;
		}

		@Override
		public String getReferenceName() {
			throw new UnsupportedOperationException();
		}

		@Override
		public ResponseEntity<String> activate(String... params) {
			requestStopOfAlgorithm();
			return new ResponseEntity<String>(HttpStatus.OK);
		}

		@Override
		public ClientWindowEffect getEffect() {
			throw new UnsupportedOperationException();
		}

		@Override
		public int getId() {
			return id;
		}

	}

	private final List<MiningAlgorithm> algorithms;
	private final DiscoveryProcess discoveryProcess;
	private final MineButtonStrategy strategy;
	private final MiningAlgorithmLaunchDialog launchDialog;
	private final Ranker ranker;
	private final int id;
	// TODO perhaps to be moved to mine button strategy once this is not an enum
	// anymore
	private final boolean showLaunchDialog;
	private final Map<Integer, Action> idToActionMap;
	private final Action requestStopOfCurrentAlgorithmAction;

	private List<Pattern> minedPatterns;

	private Future<Collection<Pattern>> future;
	private MiningAlgorithm currentAlgorithm;

	/**
	 * Only to be called from MiningSystemBuilder
	 */
	public MiningSystem(IdGenerator idGenerator,
			List<MiningAlgorithm> algorithms,
			DiscoveryProcess discoveryProcess, MineButtonStrategy strategy,
			Ranker ranker, boolean showLaunchDialog) {
		this.algorithms = algorithms;
		this.discoveryProcess = discoveryProcess;
		this.strategy = strategy;
		this.ranker = ranker;
		this.launchDialog = new MiningAlgorithmLaunchDialog(idGenerator,
				algorithms);
		this.id = idGenerator.getNextId();
		this.showLaunchDialog = showLaunchDialog;

		this.idToActionMap = new HashMap<>();
		requestStopOfCurrentAlgorithmAction = new StopMiningAlgorithmAction(
				idGenerator.getNextId());
		this.idToActionMap.put(requestStopOfCurrentAlgorithmAction.getId(),
				requestStopOfCurrentAlgorithmAction);
	}

	public List<MiningAlgorithm> getAlgorithms() {
		return algorithms;
	}

	private MineButtonStrategy getMineButtonStrategy() {
		return strategy;
	}

	public DiscoveryProcess getDiscoveryProcess() {
		return discoveryProcess;
	}

	public List<Pattern> mineClicked() throws RuntimeException {
		strategy.clicked(this, algorithms);
		return minedPatterns;
	}

	public MiningAlgorithmLaunchDialog getManualMiningModel() {
		return launchDialog;
	}

	public void requestStopOfAlgorithm() {
		LOGGER.fine("Requesting currently run algorithm to stop");
		if (currentAlgorithm != null) {
			getDiscoveryProcess().endRound(); // ??? is this the right moment to
												// end round?

			if (currentAlgorithm instanceof StoppableMiningAlgorithm) {
				((StoppableMiningAlgorithm) currentAlgorithm).requestStop();
			}
		}
	}

	public void startNextDiscoveryRoundWithCurrentAlgorithmResult()
			throws RuntimeException {
		List<Pattern> patterns = new ArrayList<>();
		try {
			patterns.addAll(future.get());
		} catch (InterruptedException | ExecutionException e) {
			LOGGER.severe("Exception while receiving result of algorithm '"
					+ e.getMessage() + "'");
			e.printStackTrace();

			future.cancel(true);
			future = null;

			throw new RuntimeException("Algorithm crashed: " + e.getMessage(),
					e);

			// System.exit(-1);
		}
		minedPatterns = ranker.rank(patterns);
	}

	public void startAlgorithm(MiningAlgorithm algorithm) {
		LOGGER.fine("starting algorithm in new thread");

		future = Executors.newSingleThreadExecutor().submit(algorithm);

		currentAlgorithm = algorithm;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getView() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Model getModel() {
		Model model = new BindingAwareModelMap();

		model.addAttribute("miningLauncherId", id);
		model.addAttribute("showLaunchDialog", showLaunchDialog);
		model.addAttribute("launchDialogTitle", getLaunchDialogTitle());
		model.addAttribute("algorithmCategoryHiddenClass",
				(getManualMiningModel().getCategories().size() == 1) ? "hidden"
						: "");

		model.addAttribute("algorithmSelectHiddenClass",
				getAlgorithms().size() == 1 ? "hidden" : "");

		model.addAttribute("requestStopOfCurrentAlgorithmActionId",
				requestStopOfCurrentAlgorithmAction.getId());

		return model;
	}

	private String getLaunchDialogTitle() {
		String launchDialogTitle;
		if (getAlgorithms().size() == 1) {
			launchDialogTitle = getAlgorithms().get(0).getName();
		} else if (launchDialog.getCategories().size() == 1) {
			launchDialogTitle = getManualMiningModel().getAlgorithmCategories()
					.get(0).getName();
		} else {
			launchDialogTitle = "Pattern Discovery";
		}
		return launchDialogTitle;
	}

	@Override
	public List<UiComponent> getComponents() {
		return Arrays.asList(launchDialog);
	}

	@Override
	public ImmutableSet<String> getOwnScriptFilenames() {
		return ImmutableSet.of("creedo-parameters.js",
				"creedo-miningrunner.js?v0.1.1.5", getMineButtonStrategy()
						.getClientJSFileName());
	}

	@Override
	public Collection<Integer> getActionIds() {
		return idToActionMap.keySet();
	}

	@Override
	public ResponseEntity<String> performAction(int id, String... params) {
		return idToActionMap.get(id).activate(params);
	}

	public void tearDown() {
		LOGGER.info("Requesting stop of currently running algorithm (if applicable), because mining system is teared down");
		requestStopOfAlgorithm();
	}

}
