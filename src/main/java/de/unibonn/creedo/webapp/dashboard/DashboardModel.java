package de.unibonn.creedo.webapp.dashboard;

import de.unibonn.creedo.ui.core.Frame;

/**
 * Interface implemented by all Dashboards that can be opened from the main
 * page.
 * 
 * @author bjacobs
 * 
 */
public interface DashboardModel extends Frame {

	public static final String DASHBOARD_ID_ATTRIBUTENAME = "analysisSessionId";

}
