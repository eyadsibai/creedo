package de.unibonn.creedo.webapp.dashboard.study;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.studies.assignmentschemes.EvaluationAssignment;
import de.unibonn.creedo.studies.state.StudyState;
import de.unibonn.creedo.ui.core.FrameBuilder;
import de.unibonn.creedo.ui.core.UiRegister.FrameCloser;
import de.unibonn.creedo.ui.core.UiRegister.IdGenerator;
import de.unibonn.creedo.webapp.dashboard.DashboardModel;
import de.unibonn.creedo.webapp.studies.rating.RatingMetric;
import de.unibonn.realkd.data.DataWorkspace;

/**
 * Represents the configuration of a rating dashboard represented by the
 * metrics, the id of the data table (old data system), and the set of results
 * to be evaluated.
 * 
 * @author Björn Jacobs
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.1.0.1
 * 
 */
public class RatingDashboardModelBuilder implements FrameBuilder {

	private List<RatingMetric> metrics = new ArrayList<>();
	private final Supplier<DataWorkspace> workspaceSupplier;
	private final StudyState studyState;
	private final EvaluationAssignment assignment;

	public RatingDashboardModelBuilder(
			Supplier<DataWorkspace> workspaceSupplier, StudyState studySate,
			EvaluationAssignment assignment) {
		this.workspaceSupplier = workspaceSupplier;
		this.studyState = studySate;
		this.assignment = assignment;
	}

	@Override
	public DashboardModel build(IdGenerator idGenerator, FrameCloser closer,
			HttpSession session) {

		return new RatingDashboardModel(idGenerator, metrics, session,
				workspaceSupplier.get(), studyState, assignment, closer);
	}

	public RatingDashboardModelBuilder setRatingMetrics(
			List<RatingMetric> metrics) {
		this.metrics = metrics;
		return this;
	}

}
