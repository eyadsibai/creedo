package de.unibonn.creedo.webapp.dashboard.patterncontainer;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.webapp.patternviews.PatternHTMLGenerator;
import de.unibonn.creedo.webapp.patternviews.ResultPatternGenerator;
import de.unibonn.realkd.patterns.Pattern;

/**
 * Wraps a core pattern adding additional field that are required for
 * manipulating and displaying patterns in a web UI. This includes a long id
 * which can be used for communication with a web browser. Moreover, it provides
 * an HTML representation of the wrapped pattern.
 * 
 * @author Bo Kang
 * 
 * @since 0.1.0
 * 
 * @version 0.1.2.1
 * 
 */
public class WebPattern {

	private final int id;

	private final Pattern pattern;

	private PatternHTMLGenerator htmlGenerator;

	private String annotationText = "";

	private final int supportSetProviderId;

	private final int actionProviderId;

	private final int openDetailedViewActionId;

	public WebPattern(int id, Pattern pattern,
			PatternHTMLGenerator htmlGenerator, int supportSetProviderId,
			int actionProvderId, int openDetailedViewActionId) {
		this.id = id;
		this.pattern = pattern;
		this.htmlGenerator = htmlGenerator;
		this.supportSetProviderId = supportSetProviderId;
		this.actionProviderId = actionProvderId;
		this.openDetailedViewActionId = openDetailedViewActionId;
	}

	public Pattern getPattern() {
		return pattern;
	}

	public int getId() {
		return id;
	}

	public String getHtml(HttpSession session) {
		return htmlGenerator.getHTML(session, this);
	}

	@Override
	public String toString() {
		return pattern.toString() + " [annotation: \"" + getAnnotationText()
				+ "\"]";
	}

	/*
	 * only to be re-set from WebDiscoveryProcess
	 */
	public void setHtmlGenerator(ResultPatternGenerator generator) {
		this.htmlGenerator = generator;
	}

	public void setAnnotationText(String annotationText) {
		this.annotationText = annotationText;
	}

	public String getAnnotationText() {
		return annotationText;
	}

	public int getSupportSetProviderId() {
		return supportSetProviderId;
	}

	public int getActionProviderId() {
		return actionProviderId;
	}

	public int getOpenDetailedViewActionId() {
		return openDetailedViewActionId;
	}
}
