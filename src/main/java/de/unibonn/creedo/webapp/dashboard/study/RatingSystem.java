package de.unibonn.creedo.webapp.dashboard.study;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.webapp.studies.rating.RatingMetric;
import de.unibonn.creedo.webapp.studies.rating.RatingOption;
import de.unibonn.creedo.webapp.studies.rating.ResultRating;

/**
 * Holds an updates ratings according to a specified set of RatingMetrics for a
 * set of objects that can be referenced by an id.
 * 
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.1.0.1
 * 
 */
public class RatingSystem {

	private final Map<Integer, List<RatingOption>> ratings = new HashMap<>();
	private final List<RatingMetric> metrics;

	public RatingSystem(List<RatingMetric> metrics) {
		this.metrics = metrics;
	}

	public void updateRating(int patternId, String metricName, int optionId) {
		List<RatingOption> ratingOfOnePattern = ratings.get(patternId);
		int metricId = 0;
		for (RatingMetric metric : metrics) {
			if (metric.getName().equals(metricName)) {
				metricId = metrics.indexOf(metric);
				break;
			}
		}
		ratingOfOnePattern.set(metricId, metrics.get(metricId)
				.getRatingOptions()[optionId]);
	}

	/**
	 * <p>
	 * Get the rating values per metric for a result pattern. A rating value is
	 * null, if this pattern is not rated using the corresponding metric.
	 * </p>
	 * 
	 * @return list of rating values.
	 * 
	 */
	public List<Integer> getPatternRatings(int patternId) {
		List<Integer> ratingOptionIds = new ArrayList<>();
		for (RatingMetric ratingMetric : metrics) {
			int ratingMetricId = metrics.indexOf(ratingMetric);
			if (ratings.get(patternId).get(ratingMetricId) == null) {
				ratingOptionIds.add(null);
			} else {
				for (int i = 0; i < ratingMetric.getRatingOptions().length; i++) {
					if (ratingMetric.getRatingOptions()[i].equals(ratings.get(
							patternId).get(ratingMetricId))) {
						ratingOptionIds.add(i);
						break;
					}
				}
			}

		}
		return ratingOptionIds;
	}


	/**
	 * <p>
	 * Initializes the rating information for one pattern (represented by its
	 * id). Creates a list with one rating score for each metric. Each score is
	 * initialized according to the default value of the corresponding metric
	 * (which can be null).
	 * </p>
	 * 
	 * @param id
	 *            the id of the pattern for which rating information is
	 *            initialized
	 */
	public void init(int id) {
		List<RatingOption> ratingOptions = new ArrayList<>();
		for (int i = 0; i < metrics.size(); i++) {
			ratingOptions.add(metrics.get(i).getDefault());
		}
		ratings.put(id, ratingOptions);
	}

	public List<RatingMetric> getMetrics() {
		return metrics;
	}

}
