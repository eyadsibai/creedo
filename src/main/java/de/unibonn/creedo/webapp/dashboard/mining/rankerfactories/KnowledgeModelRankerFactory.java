package de.unibonn.creedo.webapp.dashboard.mining.rankerfactories;

import de.unibonn.creedo.webapp.dashboard.mining.rankers.KnowledgeModelRanker;
import de.unibonn.creedo.webapp.dashboard.mining.rankers.Ranker;
import de.unibonn.realkd.data.DataWorkspace;
import de.unibonn.realkd.discoveryprocess.DiscoveryProcess;

public class KnowledgeModelRankerFactory implements RankerFactory {

	@Override
//	public Ranker getRanker(DataTable dataTable,
//			DiscoveryProcess discoveryProcess) {
	public Ranker getRanker(DataWorkspace dataWorkspace,
			DiscoveryProcess discoveryProcess) {
		// return new KnowledgeModelRanker(dataTable, discoveryProcess);
		return new KnowledgeModelRanker(dataWorkspace, discoveryProcess);
	}

	@Override
	public boolean equals(Object other) {
		return (other instanceof KnowledgeModelRankerFactory);
	}

	@Override
	public String toString() {
		return "Knowledge model ranker";
	}

}
