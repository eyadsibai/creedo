package de.unibonn.creedo.webapp.dashboard.mining;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.ui.core.UiRegister.IdGenerator;
import de.unibonn.creedo.webapp.dashboard.mining.rankerfactories.RankerFactory;
import de.unibonn.creedo.webapp.dashboard.mining.rankers.Ranker;
import de.unibonn.creedo.webapp.viewmodels.DeveloperViewModel;
import de.unibonn.realkd.algorithms.MiningAlgorithm;
import de.unibonn.realkd.algorithms.derived.DerivedAlgorithms;
import de.unibonn.realkd.algorithms.derived.ParameterTerminator;
import de.unibonn.realkd.algorithms.derived.ParameterWrapper;
import de.unibonn.realkd.algorithms.emm.dssd.DiverseSubgroupSetDiscovery;
import de.unibonn.realkd.algorithms.emm.dssd.SubgroupSetSelector;
import de.unibonn.realkd.common.optimization.BatchLogisticL1RegLinearLearner;
import de.unibonn.realkd.common.optimization.LinearFeature;
import de.unibonn.realkd.common.optimization.LinearFeatureSpace;
import de.unibonn.realkd.common.optimization.LinearModel;
import de.unibonn.realkd.common.optimization.RegressionModelFromPreferenceLearner;
import de.unibonn.realkd.common.parameter.DefaultRangeEnumerableParameter;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.data.DataWorkspace;
import de.unibonn.realkd.data.table.DataTable;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.discoveryprocess.DiscoveryProcess;
import de.unibonn.realkd.discoveryprocess.DiscoveryProcessObserver;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.emm.ContingencyTable;
import de.unibonn.realkd.patterns.emm.ContingencyTableCellKey;
import de.unibonn.realkd.patterns.emm.ContingencyTableModel;
import de.unibonn.realkd.patterns.emm.ContingencyTableModelFactory;
import de.unibonn.realkd.patterns.emm.ExceptionalModelMining;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.util.PatternUtil;

public class DssdSystemBuilder extends MiningSystemBuilder<DssdSystemBuilder> {
	private Parameter<Boolean> showLaunchDialog;

	public DssdSystemBuilder() {
		super();
		showLaunchDialog = new DefaultRangeEnumerableParameter<Boolean>("Show dialog",
				"Determines whether launch dialog is shown before execution of mining algorithm ('false' only possible with single consistently initialized algorithm).",
				Boolean.class, () -> algorithms.getCurrentValue().size() == 1
						? ImmutableList.of(new Boolean(true), new Boolean(false)) : ImmutableList.of(new Boolean(true)),
				algorithms);
		parameterContainer.addParameter(showLaunchDialog);
	}

	@Override
	public String toString() {
		return "DSSD system builder";
	}

	@Override
	protected MiningSystem concreteBuild(IdGenerator idGenerator, final DeveloperViewModel developerViewModel,
			final DataWorkspace dataWorkspace) {
		final Properties builderProperties = new Properties();
		try {
			final File conf = new File(getClass().getClassLoader().getResource("dssd_builder.properties").getFile());
			builderProperties.load(new FileReader(conf));
		} catch (IOException e) {
			throw new RuntimeException("DSSD system builder: Failed to load properties", e);
		}

		final DataTable dataTable = dataWorkspace.getAllDatatables().get(0);
		if (!dataTable.getName().equals(builderProperties.getProperty("dataset")))
			throw new RuntimeException(String.format("Dataset '%s': DSSD system builder only supports '%s'",
					dataTable.getName(), builderProperties.getProperty("dataset")));

		final DiscoveryProcess discoveryProcess = new DiscoveryProcess();

		final List<LinearFeature<Pattern>> features = new ArrayList<>();
		final String[] contingencyTableCells = builderProperties.getProperty("cells").split(",");
		for (String fid : builderProperties.getProperty("features").split(",")) {
			switch (fid) {
			case "FREQ":
				features.add(new MeasureFeature(QualityMeasureId.FREQUENCY, 1.0));
				break;
			case "DEV":
				features.add(new MeasureFeature(QualityMeasureId.TOTAL_VARIATION_DISTANCE, 1.0));
				break;
			case "CTVAL":
				for (String ctCell : contingencyTableCells)
					features.add(new ContingencyTableCellValueFeature(ctCell));
				break;
			case "CTDIFF":
				for (String ctCell : contingencyTableCells)
					features.add(new ContingencyTableCellValueFeature(ctCell));
				break;
			case "WRACC":
				for (String ctCell : contingencyTableCells)
					features.add(new ContingencyTableWRAccFeature(ctCell));
				break;
			}
		}
		final RegressionModelFromPreferenceLearner<Pattern> learner = new BatchLogisticL1RegLinearLearner<>(
				new LinearFeatureSpace<>(features, PatternUtil.ZERO_PATTERN));
		new Trainer(learner, discoveryProcess);
		developerViewModel.tellInspectable((LinearModel<Pattern>) learner.getModel());

		final DiverseSubgroupSetDiscovery coreDssd = DiverseSubgroupSetDiscovery
				.createCoreDiverseSubgroupSetDiscovery(dataWorkspace);
		final ImmutableList<Attribute<?>> targetAttributes = ImmutableList
				.of(attribute(dataTable, builderProperties.getProperty("target")));
		final Set<Attribute<?>> descriptionAttributes = new HashSet<>();
		for (String descAttrName : builderProperties.getProperty("description").split(",")) {
			descriptionAttributes.add(attribute(dataTable, descAttrName));
		}

		final double beamAlpha = Double.parseDouble(builderProperties.getProperty("beamAlpha"));
		final double postAlpha = Double.parseDouble(builderProperties.getProperty("postAlpha"));

		final double minFrequency = 0.1;
		final SubgroupSetSelector beamSelector = SubgroupSetSelector.DESCRIPTION;
		final int beamWidth = 100;
		final int intermediateResults = 10000;
		final SubgroupSetSelector postSelector = SubgroupSetSelector.DESCRIPTION;
		final int k = 100;
		final int maxDepth = 5;
		final MiningAlgorithm wrappedDssd = wrapDssdParameters(coreDssd,
				terminate(coreDssd.getDatatableParameter(), dataTable),
				terminate(coreDssd.getTargetAttributesParameter(), targetAttributes),
				terminate(coreDssd.getDescriptorAttributesParameter(), descriptionAttributes),
				terminate(coreDssd.getModelClassParameter(), ContingencyTableModelFactory.INSTANCE),
				terminate(coreDssd.getModelDistanceFunctionParameter(),
						ExceptionalModelMining.TVD_MEASUREMENT_PROCEDURE),
				terminate(coreDssd.getMinimumFrequencyThreshold(), minFrequency),
				terminate(coreDssd.getBeamSelectorParameter(), beamSelector),
				terminate(coreDssd.getBeamWidthParameter(), beamWidth),
				terminate(coreDssd.getNumberOfIntermediateResultsParameter(), intermediateResults),
				terminate(coreDssd.getPostSelectorParameter(), postSelector),
				terminate(coreDssd.getNumberOfResultsParameter(), k),
				terminate(coreDssd.getMaxDepthParameter(), maxDepth),
				terminate(coreDssd.getTargetFunctionParameter(), (pattern -> {
					final double q = learner.getModel().value(pattern);
					// System.out.printf("Quality(%s) = %.4f", pattern, q);
					return q;
				})));

		return new MiningSystem(idGenerator, ImmutableList.of(wrappedDssd), discoveryProcess,
				MineButtonStrategy.MANUALMINING,
				// new RepetitionsEliminatorFactory().getRanker(dataTable,
				// discoveryProcess), showLaunchDialog.getCurrentValue());
				new RepetitionsEliminatorFactory().getRanker(dataWorkspace, discoveryProcess),
				showLaunchDialog.getCurrentValue());
	}

	private Attribute attribute(final DataTable dataTable, final String name) {
		final int i = dataTable.getAttributeNames().indexOf(name);
		return dataTable.getAttribute(i);
	}

	private MiningAlgorithm wrapDssdParameters(DiverseSubgroupSetDiscovery dssd,
			ParameterWrapper... wrappedParameters) {
		return DerivedAlgorithms.getAlgorithmWithWrappedParameters(dssd, Arrays.asList(wrappedParameters),
				DerivedAlgorithms.EXPOSE, dssd.getName() + " with fixed parameter settings");
	}

	private <T> ParameterTerminator terminate(Parameter<T> parameter, T value) {
		return new ParameterTerminator() {
			@Override
			public Parameter<?> getWrappedParameter() {
				return parameter;
			}

			@Override
			public void setParameter() {
				parameter.set(value);
			}
		};
	}

	static final class MeasureFeature implements LinearFeature<Pattern> {
		private final QualityMeasureId measure;
		private final double defaultCoefficient;

		public MeasureFeature(final QualityMeasureId measure, final double defaultCoefficient) {
			this.measure = measure;
			this.defaultCoefficient = defaultCoefficient;
		}

		public MeasureFeature(QualityMeasureId measure) {
			this(measure, 0.0);
		}

		@Override
		public double value(final Pattern pattern) {
			return pattern.getValue(measure);
		}

		@Override
		public double getDefaultCoefficient() {
			return defaultCoefficient;
		}

		@Override
		public String toString() {
			return String.format("Value of %s", measure);
		}
	}

	static abstract class ContingencyTableFeature implements LinearFeature<Pattern> {
		protected final double defaultCoefficient;
		protected final ContingencyTableCellKey cellKey;
		protected final String cellName;

		public ContingencyTableFeature(final double defaultCoefficient, final String... keys) {
			this.defaultCoefficient = defaultCoefficient;
			this.cellKey = new ContingencyTableCellKey(Arrays.asList(keys));
			this.cellName = String.join(":", keys);
		}

		public ContingencyTableFeature(final String... keys) {
			this(0.0, keys);
		}

		protected final ContingencyTable globalTable(final Pattern p) {
			return ((ContingencyTableModel) ((ExceptionalModelPattern) p).getDescriptor().getGlobalModel())
					.getProbabilities();
		}

		protected final ContingencyTable localTable(final Pattern p) {
			return ((ContingencyTableModel) ((ExceptionalModelPattern) p).getDescriptor().getLocalModel())
					.getProbabilities();
		}

		@Override
		public double getDefaultCoefficient() {
			return defaultCoefficient;
		}
	}

	static final class ContingencyTableCellValueFeature extends ContingencyTableFeature {
		public ContingencyTableCellValueFeature(final double defaultCoefficient, final String... keys) {
			super(defaultCoefficient, keys);
		}

		public ContingencyTableCellValueFeature(final String... keys) {
			super(keys);
		}

		@Override
		public double value(final Pattern p) {
			return localTable(p).getNormalizedValue(cellKey);
		}

		@Override
		public String toString() {
			return "Local probability of " + cellName;
		}
	}

	static final class ContingencyTableCellDiffFeature extends ContingencyTableFeature {
		public ContingencyTableCellDiffFeature(final double defaultCoefficient, final String... keys) {
			super(defaultCoefficient, keys);
		}

		public ContingencyTableCellDiffFeature(final String... keys) {
			super(keys);
		}

		@Override
		public double value(final Pattern p) {
			return localTable(p).getNormalizedValue(cellKey) - globalTable(p).getNormalizedValue(cellKey);
		}

		@Override
		public String toString() {
			return "Diff of probabilities of " + cellName;
		}
	}

	static final class ContingencyTableWRAccFeature extends ContingencyTableFeature {
		public ContingencyTableWRAccFeature(final double defaultCoefficient, String... keys) {
			super(defaultCoefficient, keys);
		}

		public ContingencyTableWRAccFeature(final String... keys) {
			super(keys);
		}

		@Override
		public double value(final Pattern p) {
			return p.getValue(QualityMeasureId.FREQUENCY)
					* (localTable(p).getNormalizedValue(cellKey) - globalTable(p).getNormalizedValue(cellKey));
		}

		@Override
		public String toString() {
			return "Weighted relative accuracy for " + cellName;
		}
	}

	static final class Trainer implements DiscoveryProcessObserver {

		private static final Logger LOGGER = Logger.getLogger(Trainer.class.getName());

		private final RegressionModelFromPreferenceLearner<Pattern> learner;
		private final DiscoveryProcess discoveryProcess;

		public Trainer(final RegressionModelFromPreferenceLearner<Pattern> learner,
				final DiscoveryProcess discoveryProcess) {
			this.learner = learner;

			this.discoveryProcess = discoveryProcess;
			discoveryProcess.addObserver(this);
		}

		@Override
		public void justBeganNewRound() {
		}

		@Override
		public void roundEnded() {
			LOGGER.log(Level.INFO, "Model update triggered");
			learner.doUpdate();
			LOGGER.log(Level.INFO, "Model update done");
		}

		@Override
		public void markAsSeen(Pattern p) {
		}

		@Override
		public void aboutToSavePattern(Pattern p) {
			final List<Pattern> candidates = discoveryProcess.getDiscoveryProcessState().getCandidatePatterns();
			for (int i = 0; i < candidates.indexOf(p); i++) {
				LOGGER.log(Level.INFO,
						"'" + p.getDescriptor() + "' is preferred to '" + candidates.get(i).getDescriptor() + "'");
				learner.tellPreference(p, candidates.get(i));
			}
		}

		@Override
		public void aboutToDeletePatternFromCandidates(Pattern p) {
			final List<Pattern> candidates = discoveryProcess.getDiscoveryProcessState().getCandidatePatterns();
			for (int i = 0; i < candidates.indexOf(p); i++) {
				LOGGER.log(Level.INFO,
						"'" + candidates.get(i).getDescriptor() + "' is preferred to '" + p.getDescriptor() + "'");
				learner.tellPreference(candidates.get(i), p);
			}
		}

		@Override
		public void aboutToDeletePatternFromResults(Pattern p) {
		}
	}

	static final class RepetitionsEliminator implements Ranker {
		private final DiscoveryProcess discoveryProcess;

		public RepetitionsEliminator(final DiscoveryProcess discoveryProcess) {
			this.discoveryProcess = discoveryProcess;
		}

		@Override
		public List<Pattern> rank(final List<Pattern> patterns) {
			final List<Pattern> filtered = new ArrayList<>(patterns);
			filtered.removeAll(discoveryProcess.getDiscoveryProcessState().getDiscardedPatterns());
			filtered.removeAll(discoveryProcess.getDiscoveryProcessState().getResultPatterns());

			return filtered;
		}

		@Override
		public String toString() {
			return "RepetitionsEliminator";
		}
	}

	static final class RepetitionsEliminatorFactory implements RankerFactory {
		@Override
		// public Ranker getRanker(final DataTable dataTable,
		// final DiscoveryProcess discoveryProcess) {
		// return new RepetitionsEliminator(discoveryProcess);
		// }
		public Ranker getRanker(final DataWorkspace dataWorkspace, final DiscoveryProcess discoveryProcess) {
			return new RepetitionsEliminator(discoveryProcess);
		}
	}
}
