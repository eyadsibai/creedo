package de.unibonn.creedo.webapp;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.WebConstants;
import de.unibonn.creedo.ui.core.ActionProvider;

/**
 * Controller handles content and special page requests, which can be opened
 * from the main page.
 * 
 * @author bjacobs, mboley
 * 
 */
@Controller
public class MainPageController {

	@Autowired
	private HttpSession session;

	public MainPageController() {
		;
	}

	@RequestMapping(value = WebConstants.HOME_PATH, method = RequestMethod.GET)
	public ModelAndView index() {
		CreedoSession creedoSession = Creedo.getCreedoSession(session);

		return creedoSession.getMainFrame().getModelAndView();
	}

	@RequestMapping(value = "/login.htm", method = RequestMethod.GET)
	public String login(Model model) {
		CreedoSession creedoSession = Creedo.getCreedoSession(session);
		((ActionProvider) creedoSession
				.getMainFrame())
				.performAction(creedoSession.getLoadLoginPageActionId());
		return "redirect:showFrame.htm?frameId="
				+ Creedo.getCreedoSession(session).getMainFrame()
						.getId();
	}

}
