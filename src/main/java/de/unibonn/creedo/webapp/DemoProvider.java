package de.unibonn.creedo.webapp;

import java.util.ArrayList;
import java.util.List;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.admin.users.CreedoUser;
import de.unibonn.creedo.repositories.RepositoryEntry;
import de.unibonn.creedo.ui.indexpage.DashboardLink;
import de.unibonn.creedo.ui.indexpage.DashboardLinkBuilder;
import de.unibonn.creedo.ui.indexpage.DashboardLinkProvider;

/**
 * Compiles the list of demo links available to a user.
 * 
 * @author Björn Jacobs
 * 
 * @since 0.1.0
 * 
 * @version 0.1.2.1
 * 
 */
public class DemoProvider implements DashboardLinkProvider {

	public static DemoProvider INSTANCE = new DemoProvider();

	private DemoProvider() {
	}

	public List<DashboardLink> getDashboardLinks(CreedoUser user) {
		List<DashboardLink> results = new ArrayList<>();
		ApplicationRepositories.DEMO_REPOSITORY.getAllEntries().stream().filter(x -> x.getContent().isVisibleTo(user))
				.forEach(x -> {
					results.add(x.getContent().build(user));
				});
		// for (RepositoryEntry<String, DashboardLinkBuilder> demoEntry : ) {
		// results.add(demoEntry.getContent().build(user));
		// }
		return results;
	}

	@Override
	public String getTitle() {
		return "Interactive Demos";
	}

}
