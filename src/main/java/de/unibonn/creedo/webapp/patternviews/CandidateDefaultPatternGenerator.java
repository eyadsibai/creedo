package de.unibonn.creedo.webapp.patternviews;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.SubPopulationDescriptor;

public class CandidateDefaultPatternGenerator extends
		AbstractCandidatePatternGenerator {

	public CandidateDefaultPatternGenerator(List<String> optionalActions) {
		super(optionalActions);
	}

	@Override
	protected String getHTMLClass() {
		return "generic-pattern";
	}

	@Override
	protected String getTitle(WebPattern webPattern) {
		return "Pattern";
	}

	@Override
	protected String getTooltip(Pattern pattern) {
		StringBuffer res = new StringBuffer();
		if (pattern.getDescriptor() instanceof SubPopulationDescriptor) {
			for (Integer index : ((SubPopulationDescriptor) pattern
					.getDescriptor()).getSupportSet()) {
				res.append("<br/>");
				res.append(pattern.getDataArtifact().getObjectName(index));
			}
		}
		return res.toString();
	}

	@Override
	protected List<String> getExplanationElements(Pattern pattern) {
		return new ArrayList<>();
	}

	// @Override
	// protected String getDescription(Pattern pattern) {
	// StringBuffer res=new StringBuffer();
	// int numElements=0;
	// for (Integer index: pattern.getSupportSet()) {
	// res.append("<br/>");
	// res.append(pattern.getDataTable().getObjectName(index));
	// numElements++;
	// if (numElements==VISIBLE_DESCRIPTOR_NUMBER) break;
	// }
	// if (pattern.getSupportSet().size() > VISIBLE_DESCRIPTOR_NUMBER) {
	// res.append("<span id = \"hasMore\"><br/>... </span>");
	// }
	// return res.toString();
	// }

	@Override
	protected List<String> getDescriptorElements(Pattern pattern) {
		if (!(pattern.getDescriptor() instanceof SubPopulationDescriptor)) {
			return Arrays.asList("No description available.");
		}

		List<String> res = new ArrayList<>(
				((SubPopulationDescriptor) pattern.getDescriptor())
						.getSupportSet().size());
		for (Integer index : ((SubPopulationDescriptor) pattern.getDescriptor())
				.getSupportSet()) {
			res.add(pattern.getDataArtifact().getObjectName(index));
		}

		return res;
	}

}
