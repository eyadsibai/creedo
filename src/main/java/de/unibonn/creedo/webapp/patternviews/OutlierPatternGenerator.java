package de.unibonn.creedo.webapp.patternviews;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.List;

import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.SubPopulationDescriptor;

public class OutlierPatternGenerator extends AbstractCandidatePatternGenerator {

	public OutlierPatternGenerator(List<String> optionalActions) {
		super(optionalActions);
	}

	@Override
	protected String getHTMLClass() {
		// TODO: Is there need for an own html class here?
		return "generic-pattern";
	}

	@Override
	protected String getTitle(WebPattern webPattern) {
		return "Outlier Pattern";
	}

	@Override
	protected String getTooltip(Pattern pattern) {
		checkArgument(
				pattern.getDescriptor() instanceof SubPopulationDescriptor,
				"Pattern descriptor must describe sub population of data.");
		StringBuffer res = new StringBuffer();
		for (Integer index : ((SubPopulationDescriptor) pattern.getDescriptor())
				.getSupportSet()) {
			res.append("<br/>");
			res.append(pattern.getDataArtifact().getObjectName(index));
		}
		return res.toString();
	}

	@Override
	protected List<String> getExplanationElements(Pattern pattern) {
		return new ArrayList<>();
	}

	// @Override
	// protected String getDescription(Pattern pattern) {
	// StringBuffer res=new StringBuffer();
	// int numElements=0;
	// for (Integer index: pattern.getSupportSet()) {
	// res.append("<br/>");
	// res.append(pattern.getDataTable().getObjectName(index));
	// numElements++;
	// if (numElements==VISIBLE_DESCRIPTOR_NUMBER) break;
	// }
	// if (pattern.getSupportSet().size() > VISIBLE_DESCRIPTOR_NUMBER) {
	// res.append("<span id = \"hasMore\"><br/>... </span>");
	// }
	// return res.toString();
	// }

	@Override
	protected List<String> getDescriptorElements(Pattern pattern) {
		checkArgument(
				pattern.getDescriptor() instanceof SubPopulationDescriptor,
				"Pattern descriptor must describe subpopulation of data.");
		List<String> res = new ArrayList<>(
				((SubPopulationDescriptor) pattern.getDescriptor())
						.getSupportSet().size());
		for (Integer index : ((SubPopulationDescriptor) pattern.getDescriptor())
				.getSupportSet()) {
			res.add(((SubPopulationDescriptor) pattern.getDescriptor())
					.getDataArtifact().getObjectName(index));
		}

		return res;
	}

}
