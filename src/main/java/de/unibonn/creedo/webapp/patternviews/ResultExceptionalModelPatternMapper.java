package de.unibonn.creedo.webapp.patternviews;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.emm.DefaultModel;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.emm.GaussianModel;

public class ResultExceptionalModelPatternMapper extends
		AbstractResultPatternMapper implements PatternHTMLGenerator {

	public ResultExceptionalModelPatternMapper(List<String> optionalActions,
			AnnotationVisibility annotationVisibility) {
		super(optionalActions, annotationVisibility);
	}

	private String getTargetsString(Pattern pattern) {
		StringBuilder sb = new StringBuilder();
		for (Attribute attribute : ((ExceptionalModelPattern) pattern)
				.getDescriptor().getTargetAttributes()) {
			sb.append(attribute.getName() + ", ");
		}
		return sb.substring(0, sb.length() - 2);
	}

	@Override
	protected List<String> getDescriptionElements(Pattern pattern) {
		List<String> descriptorList = ((ExceptionalModelPattern) pattern)
				.getDescriptor().getExtensionDescriptor()
				.getElementsAsStringList();
		return descriptorList;
	}

	@Override
	protected List<String> getExplanationElements(Pattern pattern) {
		List<String> sb = new ArrayList<String>();
		// sb.append("<div class='explanation'>");

		for (QualityMeasureId measure : pattern.getMeasures()) {
			sb.add(measure.getName()
					+ String.format(": %.4f", pattern.getValue(measure)));
		}

		// sb.add(String.format(Locale.ENGLISH, " Frequency: %.4f",
		// pattern.getFrequency()));

		// sb.add("Model Dev.: "
		// + String.format(Locale.ENGLISH, "%.4f",
		// ((ExceptionalModelPattern) pattern).getModelDeviation()));

		// DefaultModel globalModel = ((ExceptionalModelPattern) pattern)
		// .getDescriptor().getGlobalModel();
		// DefaultModel localModel = ((ExceptionalModelPattern) pattern)
		// .getDescriptor().getLocalModel();
		//
		// if (globalModel instanceof GaussianModel) {
		// sb.add(String.format(Locale.ENGLISH, "Pattern Mean : %.4f",
		// ((GaussianModel) localModel).getMean()));
		// sb.add(String.format(Locale.ENGLISH, "Global Mean : %.4f",
		// ((GaussianModel) globalModel).getMean()));
		// }

		return sb;
	}

	@Override
	protected String getTitle(WebPattern webPattern) {
		return "<strong>Subgroup</strong>("
				+ getTargetsString(webPattern.getPattern()) + ")";
	}

	@Override
	protected String getHTMLClass() {
		return "subgroup";
	}
}
