package de.unibonn.creedo.webapp.patternviews;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;

public class ResultAssociationPatternMapper extends AbstractResultPatternMapper
		implements PatternHTMLGenerator {

	public ResultAssociationPatternMapper(List<String> optionalActions,
			AnnotationVisibility annotationVisibility) {
		super(optionalActions, annotationVisibility);
	}

	@Override
	protected List<String> getDescriptionElements(Pattern pattern) {
		checkArgument(pattern.getDescriptor() instanceof LogicalDescriptor,
				"Pattern must be described logically");
		LogicalDescriptor description = (LogicalDescriptor) pattern
				.getDescriptor();
		List<String> sb = new ArrayList<>(description.size());

		for (int i = 0; i < description.size(); i++) {
			sb.add(description.getElement(i)
					+ " ("
					+ String.format(Locale.ENGLISH, "%.4f", description
							.getElement(i).getSupportCount()
							/ (double) pattern.getDataArtifact().getSize())
					+ ")");
		}

		return sb;
	}

	@Override
	protected List<String> getExplanationElements(Pattern pattern) {
		List<String> sb = new ArrayList<>();
		// sb.append("<div class='explanation'>");

		for (QualityMeasureId measure : pattern.getMeasures()) {
			sb.add(measure.getName()
					+ String.format(": %.4f", pattern.getValue(measure)));
		}

		// sb.add(String.format(Locale.ENGLISH, " Frequency : %.4f",
		// pattern.getFrequency()));
		// // sb.append("<br/>");
		//
		// sb.add(String.format(Locale.ENGLISH, "Lift : %.4f\n",
		// ((Association) pattern).getLift()));
		// sb.add(String.format(Locale.ENGLISH, "Prod. Ind. Freqs. : %.4f",
		// ((Association) pattern).getProductOfIndFreqs()));

		// sb.append("</div>");

		return sb;
	}

	@Override
	protected String getTitle(WebPattern webPattern) {
		boolean patternHasPositiveLift = webPattern.getPattern().hasMeasure(
				QualityMeasureId.LIFT);
		if (patternHasPositiveLift) {
			return "<strong>Positively Associated Attribute Values</strong>";
		} else {
			return "<strong>Negatively Associated Attribute Values</strong>";
		}
	}

	@Override
	protected String getHTMLClass() {
		return "association";
	}

}
