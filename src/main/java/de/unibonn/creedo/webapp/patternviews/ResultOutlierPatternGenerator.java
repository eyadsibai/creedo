package de.unibonn.creedo.webapp.patternviews;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.SubPopulationDescriptor;
import de.unibonn.realkd.patterns.TableSubspaceDescriptor;

public class ResultOutlierPatternGenerator extends AbstractResultPatternMapper {

	public ResultOutlierPatternGenerator(List<String> optionalActions,
			AnnotationVisibility annotationVisibility) {
		super(optionalActions, annotationVisibility);
	}

	@Override
	protected String getTitle(WebPattern webPattern) {
		return "Outlier Pattern";
	}

	@Override
	protected String getHTMLClass() {
		return "generic-pattern";
	}

	@Override
	protected List<String> getDescriptionElements(Pattern pattern) {
		checkArgument(
				pattern.getDescriptor() instanceof SubPopulationDescriptor,
				"Descriptor must describe sub population.");
		checkArgument(
				pattern.getDescriptor() instanceof TableSubspaceDescriptor,
				"Descriptor must describe table subspace.");
		List<String> res = new ArrayList<>(
				((SubPopulationDescriptor) pattern.getDescriptor())
						.getSupportSet().size());
		res.add("The rows");
		for (Integer index : ((SubPopulationDescriptor) pattern.getDescriptor())
				.getSupportSet()) {
			res.add(pattern.getDataArtifact().getObjectName(index));
		}
		if (!((TableSubspaceDescriptor) pattern.getDescriptor())
				.getReferencedAttributes().isEmpty()) {
			res.add("behave annormaly in terms of "
					+ getAttributesDescriptionSubstring(pattern) + ".");
		}
		return res;
	}

	private String getAttributesDescriptionSubstring(Pattern pattern) {
		StringBuffer resultBuffer = new StringBuffer();
		Iterator<Attribute<?>> attributes = ((TableSubspaceDescriptor) pattern
				.getDescriptor()).getReferencedAttributes().iterator();
		while (attributes.hasNext()) {
			resultBuffer.append("<strong>" + attributes.next().getName()
					+ "</strong>");
			if (attributes.hasNext()) {
				resultBuffer.append(", ");
			}
		}
		return resultBuffer.toString();
	}

	// @Override
	// protected List<String> getExplanationElements(Pattern pattern) {
	// ArrayList<String> result = new ArrayList<>();
	// result.add("Frequency: " + pattern.getFrequency());
	// return result;
	// }

	/*
	 * currently duplication with other resultMappers
	 */
	@Override
	protected List<String> getExplanationElements(Pattern pattern) {
		List<String> sb = new ArrayList<>();

		for (QualityMeasureId measure : pattern.getMeasures()) {
			sb.add(measure.getName()
					+ String.format(": %.4f", pattern.getValue(measure)));
		}

		return sb;
	}

}
