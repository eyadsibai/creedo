package de.unibonn.creedo.webapp.patternviews;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.realkd.common.Pair;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.SubPopulationDescriptor;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;

public class CandidateAssociationPatternMapper extends
		AbstractCandidatePatternGenerator implements PatternHTMLGenerator {

	public CandidateAssociationPatternMapper(List<String> optionalActions) {
		super(optionalActions);
	}

	@Override
	protected String getTooltip(Pattern pattern) {
		checkArgument(pattern instanceof Association,
				"Pattern must be Association");
		checkArgument(pattern.getDescriptor() instanceof LogicalDescriptor,
				"Pattern must be described logically.");
		StringBuilder sb = new StringBuilder();

		Association localPattern = (Association) pattern;

		for (String element : ((LogicalDescriptor) localPattern.getDescriptor())
				.getElementsAsStringList()) {
			sb.append(element).append("<br/>");
		}

		sb.append("-------------------------<br/>");

		for (Pair<String, Double> measure : getMeasures(localPattern)) {
			sb.append(measure.getLhs())
					.append(": ")
					.append(String.format(Locale.ENGLISH, "%.4f",
							measure.getRhs())).append("<br/>");
		}

		return sb.toString();
	}

	/*
	 * currently this is duplication from ResultAssociationMapper
	 */
	@Override
	protected List<String> getDescriptorElements(Pattern pattern) {
		checkArgument(pattern.getDescriptor() instanceof LogicalDescriptor,
				"Assumes that pattern has logical descriptor.");
		LogicalDescriptor logicalDescriptor = (LogicalDescriptor) pattern
				.getDescriptor();
		PropositionalLogic propositionalLogic = logicalDescriptor
				.getPropositionalLogic();
		List<String> sb = new ArrayList<>(logicalDescriptor.size());

		for (int i = 0; i < logicalDescriptor.size(); i++) {
			sb.add(logicalDescriptor.getElement(i)
					+ " ("
					+ String.format(Locale.ENGLISH, "%.4f", propositionalLogic.getSupportSet(logicalDescriptor
							.getElement(i).getId()).size()
							/ (double) propositionalLogic
									.getSize())
					// .getPropositionalLogic().getSize())
					+ ")");
		}

		return sb;
		/*
		 * Code for fancy descriptors:
		 */
		// return pattern
		// .getDescriptor()
		// .getDescriptionElements()
		// .stream()
		// .map(CandidateAssociationPatternMapper::descriptionElementToString)
		// .collect(Collectors.toList());
	}

	// private static String descriptionElementToString(
	// DescriptionElement descriptionElement) {
	// String result = "";
	// if (descriptionElement.getTags().contains(
	// DescriptionElementTag.DATA_REFERENCE)) {
	// result += "<em>";
	// }
	// result += descriptionElement.getContent().toString();
	// if (descriptionElement.getTags().contains(
	// DescriptionElementTag.DATA_REFERENCE)) {
	// result += "</em>";
	// }
	// return result;
	// }

	@Override
	protected List<String> getExplanationElements(Pattern pattern) {
		checkArgument(
				pattern.getDescriptor() instanceof SubPopulationDescriptor,
				"Pattern descriptor must describe sub population of data.");
		List<String> result = new ArrayList<>();

		result.add("<b>occur together in</b> "
				+ ((SubPopulationDescriptor) pattern.getDescriptor())
						.getSupportSet().size()
				+ " rows"
				+ (pattern.hasMeasure(QualityMeasureId.FREQUENCY) ? String
						.format(Locale.ENGLISH, " (freq. %.3f)",
								pattern.getValue(QualityMeasureId.FREQUENCY))
						: ""));

		result.add(String.format(Locale.ENGLISH,
				"<b>compared to</b> %.1f rows, which are ",
				((Association) pattern).getExpectedFrequency()
						* ((LogicalDescriptor) pattern.getDescriptor())
								.getPropositionalLogic().getSize())
				+ String.format(Locale.ENGLISH,
						"expected assuming independence (lift %.3f)",
						((Association) pattern).getLift()));

		return result;
	}

	public static List<Pair<String, Double>> getMeasures(Association pattern) {
		List<Pair<String, Double>> results = new ArrayList<>();

		for (QualityMeasureId measure : pattern.getMeasures()) {
			results.add(new Pair<>(measure.getName(), pattern.getValue(measure)));
		}

		return results;
	}

	@Override
	protected String getTitle(WebPattern webPattern) {
		// double liftMeasurement = ((Association)
		// webPattern.getPattern()).getLift();
		boolean patternHasPositiveLift = webPattern.getPattern().hasMeasure(
				QualityMeasureId.LIFT);
		if (patternHasPositiveLift) {
			return "Positively Associated Attribute Values";
		} else {
			return "Negatively Associated Attribute Values";
		}
	}

	@Override
	protected String getHTMLClass() {
		return "association";
	}

}
