package de.unibonn.creedo.webapp.patternviews;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.QualityMeasureId;
import de.unibonn.realkd.patterns.SubPopulationDescriptor;
import de.unibonn.realkd.patterns.TableSubspaceDescriptor;

public class ResultDefaultPatternGenerator extends AbstractResultPatternMapper {

	public ResultDefaultPatternGenerator(List<String> optionalActions,
			AnnotationVisibility annotationVisibility) {
		super(optionalActions, annotationVisibility);
	}

	@Override
	protected String getTitle(WebPattern webPattern) {
		return "Pattern";
	}

	@Override
	protected String getHTMLClass() {
		return "generic-pattern";
	}

	@Override
	protected List<String> getDescriptionElements(Pattern pattern) {

		if (pattern.getDescriptor() instanceof SubPopulationDescriptor) {
			return getDescriptionElements((SubPopulationDescriptor) pattern
					.getDescriptor());
			// res.add("The rows");
			// for (Integer index : pattern.getSupportSet()) {
			// res.add(pattern.getDataArtifact().getObjectName(index));
			// }
			// if (!pattern.getAttributes().isEmpty()) {
			// res.add("have a notable behavior in term of "
			// + getAttributesDescriptionSubstring(pattern) + ".");
			// }
		}
		List<String> res = new ArrayList<>();
		res.add("No description available.");
		return res;
	}

	private List<String> getDescriptionElements(
			SubPopulationDescriptor descriptor) {
		List<String> res = new ArrayList<>(descriptor.getSupportSet().size());

		res.add("The rows");
		for (Integer index : descriptor.getSupportSet()) {
			res.add(descriptor.getDataArtifact().getObjectName(index));
		}
		if ((descriptor instanceof TableSubspaceDescriptor)
				&& !((TableSubspaceDescriptor) descriptor)
						.getReferencedAttributes().isEmpty()) {
			res.add("have a notable behavior in term of "
					+ getAttributesDescriptionSubstring(((TableSubspaceDescriptor) descriptor)
							.getReferencedAttributes()) + ".");
		} else {
			res.add("stick out.");
		}

		return res;
	}

	private String getAttributesDescriptionSubstring(
			List<Attribute<?>> attributes) {
		StringBuffer resultBuffer = new StringBuffer();
		Iterator<Attribute<?>> attributeIterator = attributes.iterator();
		while (attributeIterator.hasNext()) {
			resultBuffer.append("<strong>" + attributeIterator.next().getName()
					+ "</strong>");
			if (attributeIterator.hasNext()) {
				resultBuffer.append(", ");
			}
		}
		return resultBuffer.toString();
	}

	// @Override
	// protected List<String> getExplanationElements(Pattern pattern) {
	// ArrayList<String> result = new ArrayList<>();
	// result.add("Frequency: " + pattern.getFrequency());
	// return result;
	// }

	/*
	 * currently duplication with other resultMappers
	 */
	@Override
	protected List<String> getExplanationElements(Pattern pattern) {
		List<String> sb = new ArrayList<>();

		for (QualityMeasureId measure : pattern.getMeasures()) {
			sb.add(measure.getName()
					+ String.format(": %.4f", pattern.getValue(measure)));
		}

		return sb;
	}

}
