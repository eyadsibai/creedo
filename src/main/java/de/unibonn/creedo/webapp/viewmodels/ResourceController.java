package de.unibonn.creedo.webapp.viewmodels;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import de.unibonn.creedo.ConfigurationProperties;
import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.ui.ResourceUploadPage;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.utils.Mimetypes;
import de.unibonn.creedo.webapp.CreedoSession;

/**
 * Controller that serves and stores content from the resources folder,
 * especially html files and images.
 *
 * @author Björn Jacobs
 * 
 * @since 0.1.0
 * 
 * @version 0.3.0
 * 
 */
@Controller
// @RequestMapping(value = "/")
public class ResourceController {

	private final Logger LOGGER = Logger.getLogger(ResourceController.class.getName());

	private final Pattern filenamePattern = Pattern.compile("^[\\.0-9a-zA-Z_-]+$");

	@Autowired
	private HttpSession httpSession;

	@RequestMapping(value = "file.htm")
	public ResponseEntity<FileSystemResource> serveImage(@RequestParam("filename") String filename) {
		Matcher matcher = filenamePattern.matcher(filename);

		if (!matcher.matches()) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		} else {
			String fullpath = ConfigurationProperties.get().USER_FOLDER + "/" + filename;

			try {
				Path path = FileSystems.getDefault().getPath(fullpath);
				String s = Files.probeContentType(path);

				// as of current Java8 version (1.8.0_65-b17) above method fails
				// at least on Mac OS X. Hence, here is a fallback case - Mario
				if (s == null) {
					s = Mimetypes.getFileTypeMap().getContentType(filename);
				}

				HttpHeaders responseHeaders = new HttpHeaders();
				if (s != null) {
					responseHeaders.setContentType(MediaType.parseMediaType(s));
				} else {
					// as of version 0.3.0 this cannot be entered as long as the
					// default Java type map always returns
					// application/octet-stream as a fallback. However, as we
					// might resort to a different mime detectio library in the
					// future, I leave this case in order to not potentially
					// miss detection failures - Mario.
					LOGGER.warning("MIME type of " + filename
							+ " could not be detected; serving without content type information.");
				}
				return new ResponseEntity<>(new FileSystemResource(fullpath), responseHeaders, HttpStatus.OK);
			} catch (IOException e) {
				return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
			}
		}

	}

	/**
	 * Handler that delegates the post request content to the ResourceUploadPage
	 * component which in turn is responsible for storing the content.
	 *
	 * @see ResourceUploadPage
	 */
	@RequestMapping(value = "uploadResource.htm", method = RequestMethod.POST)
	public String uploadResource(@RequestParam("componentId") Integer componentId,
			@RequestParam("file") MultipartFile[] files,
			@RequestHeader(value = "referer", required = false) final String referer) {

		try {
			CreedoSession creedoSession = Creedo.getCreedoSession(httpSession);
			UiComponent uiComponent = creedoSession.getUiComponent(componentId);
			((ResourceUploadPage) uiComponent).fileUploadPerformed(files);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "redirect:" + referer;
	}

}
