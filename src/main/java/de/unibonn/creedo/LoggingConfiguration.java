package de.unibonn.creedo;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Configures and keeps references to loggers based on
 * {@link ConfigurationProperties}. This should be created through Spring on
 * application startup after configuration properties have been read.
 * 
 * @author Mario Boley
 * 
 * @since 0.1.2
 * 
 * @version 0.1.2.1
 * 
 */
public class LoggingConfiguration {

	private static final Logger LOGGER = Logger
			.getLogger(LoggingConfiguration.class.getName());

	private final List<Logger> loggerWithAllLevel;

	private final Logger appLogger;

	private final Logger rootLogger;

	public LoggingConfiguration() {
		loggerWithAllLevel = new ArrayList<>();

		rootLogger = Logger.getLogger("");
		rootLogger
				.setLevel(ConfigurationProperties.get().GENERAL_DEFAULT_LOG_LEVEL);

		appLogger = Logger.getLogger("de.unibonn");
		appLogger.setUseParentHandlers(false);
		ConsoleHandler handler = new ConsoleHandler();
		handler.setLevel(Level.ALL);
		appLogger.addHandler(handler);
		appLogger.setLevel(ConfigurationProperties.get().APP_DEFAULT_LOG_LEVEL);

		for (String logDomain : ConfigurationProperties.get().LOG_DOMAINS_WITH_LEVEL_ALL) {
			Logger logger = Logger.getLogger(logDomain);
			logger.setLevel(Level.ALL);
			LOGGER.info("Set logger for domain '" + logDomain
					+ "' to level 'ALL'");
			loggerWithAllLevel.add(logger);
		}
	}
}