package de.unibonn.creedo.common;

import javax.servlet.ServletContext;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.context.ServletContextAware;

/**
 * <p>
 * Provides static accessors for the spring application context as well as the
 * web context. These are both initialized by Spring on application startup
 * through a singleton bean of the nested type ContextReceiver.
 * </p>
 * 
 * @author Pavel Tokmakov
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.1.0
 *
 */
public class ContextProvider {

	public static class ContextReceiver implements ApplicationContextAware,
			ServletContextAware {

		@Override
		public void setApplicationContext(ApplicationContext applicationContext)
				throws BeansException {
			ContextProvider.setApplicationContext(applicationContext);
		}

		@Override
		public void setServletContext(ServletContext arg0) {
			ContextProvider.setServletContext(arg0);
		}

	}

	private static ApplicationContext applicationContext;

	private static ServletContext servletContext;

	public static void setApplicationContext(ApplicationContext ctx) {
		applicationContext = ctx;
	}

	public static ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public static void setServletContext(ServletContext servletContext) {
		ContextProvider.servletContext = servletContext;
	}

	public static ServletContext getServletContext() {
		return servletContext;
	}

}
