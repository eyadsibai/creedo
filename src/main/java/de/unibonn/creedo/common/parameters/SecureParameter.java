package de.unibonn.creedo.common.parameters;

import java.util.function.Function;

import de.unibonn.creedo.common.BCrypt;
import de.unibonn.realkd.common.parameter.DefaultParameter;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ValueValidator;

public interface SecureParameter<T> extends Parameter<T> {

	// public static SecureParameter<String> secureStringParameter(String name,
	// String description, String initValue,
	// ValueValidator<? super String> validator, String helperText) {
	//
	// class SecureParameterImpl extends DefaultParameter<String> implements
	// SecureParameter<String> {
	//
	// public SecureParameterImpl(String name, String description, Class<?>
	// type, String initValue,
	// Function<String, String> parser, ValueValidator<? super String>
	// validator, String hint,
	// Parameter<?>... dependsOnParameters) {
	// super(name, description, type, initValue, parser, validator, hint,
	// dependsOnParameters);
	// }
	//
	// }
	//
	// return new SecureParameterImpl(name, description, String.class,
	// initValue, input -> BCrypt.hashpw(input, BCrypt.gensalt(12)), validator,
	// helperText);
	// }

	public void setByClearValue(String clearValue);

}
