package de.unibonn.creedo;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.stereotype.Component;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.common.ContextProvider;
import de.unibonn.realkd.util.StringUtils;

/**
 * Central entity that represents the configuration entries in the user-provided
 * properties file at runtime.
 *
 * @author Björn Jacobs
 * 
 * @since 0.1.0
 * 
 * @version 0.3.0
 * 
 */
@Component
public class ConfigurationProperties {

	private static final Logger LOGGER = Logger.getLogger(ConfigurationProperties.class.getName());

	public static ConfigurationProperties get() {
		return (ConfigurationProperties) ContextProvider.getApplicationContext().getBean("configurationProperties");
	}

	private static final Level GENERAL_DEFAULT_LOG_LEVEL_DEFAULT = Level.WARNING;
	private static final Level APP_DEFAULT_LOG_LEVEL_DEFAULT = Level.INFO;

	public final String USER_FOLDER;
	public final String CREEDO_DB_DRIVER;
	public final String CREEDO_DB_SYSTEM;
	public final String CREEDO_DB_HOST;
	public final String CREEDO_DB_PORT;
	public final String CREEDO_DB_SCHEMA;
	public final String CREEDO_DB_USER;
	public final String CREEDO_DB_PASS;
	public final String DEFAULT_ROOT_PASSWORD;

	/**
	 * The key for the init-resource subfolder used for data initialization.
	 */
	public final String CREEDO_ACTIVE_PLUGIN;
	public final List<String> MINING_SYSTEM_BUILDER_CLASS_NAMES;
	public final List<Class<?>> MINING_SYSTEM_BUILDER_CLASSES;
	public final Level GENERAL_DEFAULT_LOG_LEVEL;
	public final Level APP_DEFAULT_LOG_LEVEL;
	public final List<String> LOG_DOMAINS_WITH_LEVEL_ALL;

	public ConfigurationProperties(Properties properties) {
		GENERAL_DEFAULT_LOG_LEVEL = determineLogLevel(properties.getProperty("creedo.logging.level.default.general"),
				GENERAL_DEFAULT_LOG_LEVEL_DEFAULT);
		APP_DEFAULT_LOG_LEVEL = determineLogLevel(properties.getProperty("creedo.logging.level.default.app"),
				APP_DEFAULT_LOG_LEVEL_DEFAULT);
		String logDomainString = properties.getProperty("creedo.logging.level.all");
		List<String> logDomainStringList = ImmutableList.of();
		if (logDomainString != null) {
			try {
				logDomainStringList = StringUtils.jsonArrayToStringList(logDomainString);
			} catch (Exception exception) {
				LOGGER.warning("Could not parse list of log domains with level 'all' from '" + logDomainString + "'");
			}
		}
		LOG_DOMAINS_WITH_LEVEL_ALL = logDomainStringList;

		List<String> providedClassNames = StringUtils
				.jsonArrayToStringList(properties.getProperty("creedo.app.miningsystembuilders"));

		List<Class<?>> foundClasses = new ArrayList<Class<?>>();
		List<String> foundClassNames = new ArrayList<String>();
		for (String name : providedClassNames) {
			try {
				foundClasses.add(Class.forName(name));
				foundClassNames.add(name);
			} catch (ClassNotFoundException e) {
				LOGGER.log(Level.WARNING, "Could not load mining builder class '" + name + "'");
			}
		}
		MINING_SYSTEM_BUILDER_CLASSES = ImmutableList.copyOf(foundClasses);
		MINING_SYSTEM_BUILDER_CLASS_NAMES = ImmutableList.copyOf(foundClassNames);

		// Root
		CREEDO_ACTIVE_PLUGIN = properties.getProperty("creedo.root.activeplugin");
		USER_FOLDER = properties.getProperty("creedo.root.userfolder");
		DEFAULT_ROOT_PASSWORD = properties.getProperty("creedo.root.defaultrootpassword", "");

		// Database
		CREEDO_DB_DRIVER = properties.getProperty("creedo.db.driver");
		CREEDO_DB_SYSTEM = properties.getProperty("creedo.db.system");
		CREEDO_DB_HOST = properties.getProperty("creedo.db.host");
		CREEDO_DB_PORT = properties.getProperty("creedo.db.port");
		CREEDO_DB_SCHEMA = properties.getProperty("creedo.db.schema");
		CREEDO_DB_USER = properties.getProperty("creedo.db.user");
		CREEDO_DB_PASS = properties.getProperty("creedo.db.pass");
	}

	private Level determineLogLevel(String defaultLogLevelString, Level defaultLevel) {
		Level result = defaultLevel;
		if (defaultLogLevelString != null) {
			try {
				Level parsedLevel = Level.parse(defaultLogLevelString);
				result = parsedLevel;
			} catch (IllegalArgumentException illegalArgumentException) {
				LOGGER.warning("Could not parse specified log level '" + defaultLogLevelString + "'; using default '"
						+ defaultLevel + "'");
			}
		} else {
			LOGGER.info("No log level specified; using default '" + defaultLevel + "'");
		}
		return result;
	}
}
