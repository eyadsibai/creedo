package de.unibonn.creedo.ui;

import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.ui.core.PageContainer;

/**
 * Page implementation that displays arbitrary html content.
 *
 * @author Björn Jacobs
 * 
 * @since 0.1.0
 * 
 * @version 0.1.0.1
 * 
 * @see PageContainer
 * 
 */
public class HtmlPage implements Page {

	private final BindingAwareModelMap model;

	private final String title;

	private final String referenceName;

	public HtmlPage(String title, String referenceName,
			String contentPageContentHtml) {
		model = new BindingAwareModelMap();
		this.title = title;
		this.referenceName = referenceName;
		model.addAttribute("mainContent", contentPageContentHtml);
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public String getViewImport() {
		return "contentPage.jsp";
	}

	@Override
	public Model getModel() {
		return model;
	}

	@Override
	public String getReferenceName() {
		return referenceName;
	}

}
