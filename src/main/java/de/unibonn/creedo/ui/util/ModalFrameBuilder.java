package de.unibonn.creedo.ui.util;

import java.util.function.Supplier;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.ui.core.Frame;
import de.unibonn.creedo.ui.core.FrameBuilder;
import de.unibonn.creedo.ui.core.UiRegister.FrameCloser;
import de.unibonn.creedo.ui.core.UiRegister.IdGenerator;

/**
 * Builder that, on build, delivers one of two possible products depending on a
 * specified boolean condition.
 * 
 * @author Mario Boley
 *
 * @since 0.1.2
 * 
 * @version 0.1.2.1
 *
 */
public class ModalFrameBuilder implements FrameBuilder {

	private final Supplier<Boolean> test;
	private final FrameBuilder builderIfFalse;
	private final FrameBuilder builderIfTrue;

	public ModalFrameBuilder(Supplier<Boolean> test,
			FrameBuilder builderIfTrue, FrameBuilder builderIfFalse) {
		this.test = test;
		this.builderIfTrue = builderIfTrue;
		this.builderIfFalse = builderIfFalse;
	}

	@Override
	public Frame build(IdGenerator idGenerator, FrameCloser closer, HttpSession session) {
		if (test.get()) {
			return builderIfTrue.build(idGenerator, closer, session);
		} else {
			return builderIfFalse.build(idGenerator, closer, session);
		}
	}

}
