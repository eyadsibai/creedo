package de.unibonn.creedo.ui.standard;

import static com.google.common.base.Preconditions.checkNotNull;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.UiRegister;
import de.unibonn.creedo.ui.core.UiRegister.FrameCloser;

/**
 * Action that closes a frame through a frame closer that is issued by
 * {@link UiRegister} during frame construction.
 * 
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.1.2.1
 *
 */
public class CloseAction implements Action {

	private final FrameCloser closer;

	private final int id;

	public CloseAction(int id, FrameCloser closer) {
		checkNotNull(closer);
		this.id = id;
		this.closer = closer;
	}

	@Override
	public String getReferenceName() {
		return "Close";
	}

	@Override
	public ResponseEntity<String> activate(String... params) {
		closer.requestClose();
		return new ResponseEntity<String>(HttpStatus.OK);
	}

	@Override
	public ClientWindowEffect getEffect() {
		return Action.ClientWindowEffect.CLOSE;
	}

	@Override
	public int getId() {
		return id;
	}
}