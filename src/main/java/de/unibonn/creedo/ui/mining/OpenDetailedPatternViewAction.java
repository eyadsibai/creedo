package de.unibonn.creedo.ui.mining;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.support.BindingAwareModelMap;

import de.unibonn.creedo.Creedo;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.Frame;
import de.unibonn.creedo.webapp.dashboard.patterncontainer.WebPattern;
import de.unibonn.creedo.webapp.patternviews.CandidateAssociationPatternMapper;
import de.unibonn.creedo.webapp.patternviews.CandidateExceptionalModelPatternMapper;
import de.unibonn.creedo.webapp.utils.VisualizationProvider;
import de.unibonn.realkd.data.table.attribute.Attribute;
import de.unibonn.realkd.patterns.Pattern;
import de.unibonn.realkd.patterns.SubPopulationDescriptor;
import de.unibonn.realkd.patterns.TableSubspaceDescriptor;
import de.unibonn.realkd.patterns.association.Association;
import de.unibonn.realkd.patterns.emm.ExceptionalModelPattern;
import de.unibonn.realkd.patterns.logical.LogicalDescriptor;
import de.unibonn.realkd.patterns.outlier.Outlier;

/**
 * Action creates and registers a new frame object for showing a detailed view
 * of a pattern within a pattern map. Action returns the url for showing this
 * frame.
 * 
 * @author Mario Boley
 * 
 * @since 0.1.2
 * 
 * @version 0.1.2.1
 *
 */
public class OpenDetailedPatternViewAction implements Action {

	private final int id;

	private final Map<Integer, WebPattern> idToPatternMap;

	private final HttpSession httpSession;

	public OpenDetailedPatternViewAction(int id,
			Map<Integer, WebPattern> idToPatternMap, HttpSession httpSession) {
		this.id = id;
		this.idToPatternMap = idToPatternMap;
		this.httpSession = httpSession;
	}

	@Override
	public String getReferenceName() {
		throw new UnsupportedOperationException();
	}

	@Override
	public ResponseEntity<String> activate(String... params) {
		if (params.length != 1) {
			return new ResponseEntity<String>(
					"Expect exactly one pattern id parameter",
					HttpStatus.BAD_REQUEST);
		}
		int patternId = Integer.parseInt(params[0]);
		WebPattern webPattern = idToPatternMap.get(patternId);

		List<String> vImages = VisualizationProvider
				.createDetailedPatternImages(httpSession, webPattern);

		BindingAwareModelMap mav = new BindingAwareModelMap();
		mav.addAttribute("vImages", vImages);
		mav.addAttribute("showNavBar", true);

		if (webPattern.getPattern() instanceof ExceptionalModelPattern) {
			ExceptionalModelPattern emmPattern = (ExceptionalModelPattern) webPattern
					.getPattern();
			String annotation = webPattern.getAnnotationText();
			return getEmmPatternVisualization(mav, emmPattern, annotation);
		} else if (webPattern.getPattern() instanceof Association) {
			Association assocPattern = (Association) webPattern.getPattern();
			String annotation = webPattern.getAnnotationText();
			return getAssociationPatternVisualization(mav, assocPattern,
					annotation);
		} else if (webPattern.getPattern() instanceof Outlier) {
			Outlier outlier = (Outlier) webPattern.getPattern();
			String annotation = webPattern.getAnnotationText();
			return getOutlierPatternVisualization(mav, outlier);
		} else {
			return getGenericPatternVisualization(mav, webPattern.getPattern());
		}
	}

	private ResponseEntity<String> getEmmPatternVisualization(
			BindingAwareModelMap modelMap, ExceptionalModelPattern pattern,
			String annotation) {
		// BindingAwareModelMap modelMap = new BindingAwareModelMap();
		modelMap.addAttribute("targets", pattern.getDescriptor()
				.getTargetAttributes());
		modelMap.addAttribute("propositions", pattern.getDescriptor()
				.getExtensionDescriptor().getElements());
		modelMap.addAttribute("measures",
				CandidateExceptionalModelPatternMapper.getMeasures(pattern));
		modelMap.addAttribute("annotation", annotation);

		Frame frame = Creedo.getCreedoSession(httpSession).getUiRegister()
				.createStaticFrame(modelMap, "patterns/extendedEmmPatternView");

		return new ResponseEntity<String>("showFrame.htm?frameId="
				+ frame.getId(), HttpStatus.OK);
	}

	private ResponseEntity<String> getAssociationPatternVisualization(
			BindingAwareModelMap mav, Association pattern, String annotation) {
		// BindingAwareModelMap mav = new BindingAwareModelMap();
		mav.addAttribute("propositions",
				((LogicalDescriptor) pattern.getDescriptor()).getElements());
		mav.addAttribute("measures",
				CandidateAssociationPatternMapper.getMeasures(pattern));
		mav.addAttribute("annotation", annotation);

		Frame frame = Creedo.getCreedoSession(httpSession).getUiRegister()
				.createStaticFrame(mav, "patterns/extendedAssociationView");

		return new ResponseEntity<String>("showFrame.htm?frameId="
				+ frame.getId(), HttpStatus.OK);
	}

	private ResponseEntity<String> getOutlierPatternVisualization(
			BindingAwareModelMap mav, Outlier pattern) {
		checkArgument(
				pattern.getDescriptor() instanceof SubPopulationDescriptor,
				"Descriptor must describe sub population.");
		checkArgument(
				pattern.getDescriptor() instanceof TableSubspaceDescriptor,
				"Descriptor must describe table subspace.");
		// BindingAwareModelMap mav = new BindingAwareModelMap();
		List<String> elementNames = new ArrayList<String>();
		for (Integer rowIndex : ((SubPopulationDescriptor) pattern
				.getDescriptor()).getSupportSet()) {
			elementNames.add(pattern.getDatatable().getObjectName(rowIndex));
		}
		mav.addAttribute("elementNames", elementNames);
		mav.addAttribute("attributes", ((TableSubspaceDescriptor) pattern
				.getDescriptor()).getReferencedAttributes());

		// mav.addObject("measures",
		// pattern.);

		Frame frame = Creedo.getCreedoSession(httpSession).getUiRegister()
				.createStaticFrame(mav, "patterns/outlierPatternView");

		return new ResponseEntity<String>("showFrame.htm?frameId="
				+ frame.getId(), HttpStatus.OK);
	}

	private ResponseEntity<String> getGenericPatternVisualization(
			BindingAwareModelMap mav, Pattern pattern) {
		// BindingAwareModelMap mav = new BindingAwareModelMap();
		List<String> elementNames = new ArrayList<String>();
		List<Attribute> attributes = new ArrayList<Attribute>();

		if (pattern.getDescriptor() instanceof SubPopulationDescriptor) {
			for (Integer rowIndex : ((SubPopulationDescriptor) pattern
					.getDescriptor()).getSupportSet()) {
				elementNames.add(pattern.getDataArtifact().getObjectName(
						rowIndex));
			}
		}

		if (pattern.getDescriptor() instanceof TableSubspaceDescriptor) {
			attributes.addAll(((TableSubspaceDescriptor) pattern
					.getDescriptor()).getReferencedAttributes());
		}

		mav.addAttribute("elementNames", elementNames);
		mav.addAttribute("attributes", attributes);

		// mav.addObject("measures",
		// pattern.);

		Frame frame = Creedo.getCreedoSession(httpSession).getUiRegister()
				.createStaticFrame(mav, "patterns/extendedGenericPatternView");

		return new ResponseEntity<String>("showFrame.htm?frameId="
				+ frame.getId(), HttpStatus.OK);
	}

	@Override
	public ClientWindowEffect getEffect() {
		return ClientWindowEffect.POPUP;
	}

	@Override
	public int getId() {
		return id;
	}

}
