package de.unibonn.creedo.ui.mining;

import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import de.unibonn.creedo.ui.core.DataProvider;
import de.unibonn.creedo.ui.core.UiComponent;
import de.unibonn.creedo.webapp.viewmodels.DataTableViewModel;
import de.unibonn.creedo.webapp.viewmodels.MetaDataTableModel;
import de.unibonn.creedo.webapp.viewmodels.PointCloudViewModel;
import de.unibonn.realkd.data.DataWorkspace;
import de.unibonn.realkd.data.propositions.PropositionalLogic;
import de.unibonn.realkd.data.propositions.TableBasedPropositionalLogic;
import de.unibonn.realkd.data.table.DataTable;

/**
 * UI component that aggregates several views on a data workspace.
 * 
 * @author Mario Boley
 * @author Elvin Efendijev
 * 
 * @since 0.1.0
 * 
 * @version 0.1.2.1
 *
 */
public class DataViewContainer implements UiComponent, DataProvider {

	private final int id;

	private DataTableViewModel dataTableViewModel;

	private MetaDataTableModel metaDataModel;

	private PointCloudViewModel pointCloudViewModel;

	private final DataWorkspace dataWorkspace;

	private final HttpSession httpSession;

	private final DataWorkSpaceTooltip tooltip;

	public DataViewContainer(int id, HttpSession httpSession,
			DataWorkspace dataWorkspace) {
		this.id = id;
		this.httpSession = httpSession;
		this.dataWorkspace = dataWorkspace;
		this.tooltip = new DataWorkSpaceTooltip(dataWorkspace);
	}

	public DataTable getDataTable() {
		return dataWorkspace.getAllDatatables().get(0);
	}

	public PropositionalLogic getPropositionalLogic() {
		return dataWorkspace.getAllPropositionalLogics().get(0);
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getView() {
		return "../static/dataViewContainer.jsp";
	}

	@Override
	public Model getModel() {
		BindingAwareModelMap model = new BindingAwareModelMap();
		model.addAttribute("datasetName", getDataTable().getName());
		model.addAttribute("dataViewContainerId", this.getId());
		model.addAttribute("dataTableViewModel", getDataTableViewModel());
		model.addAttribute("metaDataModel", getMetaDataModel());
		return model;
	}

	@Override
	public List<UiComponent> getComponents() {
		return Arrays.asList();
	}

	private PointCloudViewModel getPointCloudViewModel() {
		if (pointCloudViewModel == null) {
			pointCloudViewModel = new PointCloudViewModel(getDataTable());
		}
		return pointCloudViewModel;
	}

	private DataTableViewModel getDataTableViewModel() {
		if (dataTableViewModel == null) {
			dataTableViewModel = new DataTableViewModel(getDataTable(),
					httpSession);
		}
		return dataTableViewModel;
	}

	private MetaDataTableModel getMetaDataModel() {
		if (metaDataModel == null) {
			if (getPropositionalLogic() instanceof TableBasedPropositionalLogic) {
				metaDataModel = new MetaDataTableModel(
						(TableBasedPropositionalLogic) getPropositionalLogic());
			}
		}
		return metaDataModel;
	}

	@Override
	public ImmutableSet<String> getOwnScriptFilenames() {
		return ImmutableSet.of("creedo-dataviewcontainer.js?v=0.1.2.19");
	}

	@Override
	public ImmutableSet<String> getDataItemIds() {
		return ImmutableSet.of("tableData", "columnTooltips", "pointData",
				"description");
	}

	private static class DataWorkSpaceTooltip {

		String tooltip;

		public DataWorkSpaceTooltip(DataWorkspace dataWorkspace) {
			StringBuilder tooltipBuilder = new StringBuilder();
			tooltipBuilder.append("<p>"
					+ dataWorkspace.getAllDatatables().get(0).getDescription()
					+ "</p>");
			tooltipBuilder.append("<hr class='no-margin' />");
			tooltipBuilder.append("<br />");
			tooltipBuilder.append("<p>");
			tooltipBuilder.append(dataWorkspace.getAllDatatables().get(0)
					.getSize()
					+ " rows<br />\n");
			tooltipBuilder.append(dataWorkspace.getAllDatatables().get(0)
					.getNumberOfAttributes()
					+ " attributes<br />\n");
			tooltipBuilder.append(dataWorkspace.getAllPropositionalLogics()
					.get(0).getPropositions().size()
					+ " propositions<br />\n");
			tooltipBuilder.append("</p>");
			tooltip = tooltipBuilder.toString();
		}

		public String toString() {
			return tooltip;
		}

	}

	/**
	 * <p>
	 * <b>Item 'tableData':</b> returns the data in the DataTable in its
	 * original order. Do not change the order here since it will break the
	 * data-consistency with clients. <br>
	 * FUTURE: implement server side processing for DataTables:
	 * http://datatables.net/manual/server-side (elvin)
	 * </p>
	 * 
	 */
	@Override
	public ImmutableList<Object> getDataItem(String dataItem) {
		if (dataItem.equals("tableData")) {
			return ImmutableList.copyOf(getDataTableViewModel()
					.getDataTableContent());
		} else if (dataItem.equals("pointData")) {
			return ImmutableList.copyOf(getPointCloudViewModel().getPoints());
		} else if (dataItem.equals("columnTooltips")) {
			return ImmutableList.copyOf(getDataTableViewModel()
					.getColumnToolTipHtml());
		} else if (dataItem.equals("description")) {
			return ImmutableList.of(tooltip.toString());
		} else {
			throw new IllegalArgumentException("No such data item.");
		}
	}

}
