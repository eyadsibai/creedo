package de.unibonn.creedo.ui;

import static de.unibonn.realkd.common.parameter.Parameters.stringParameter;

import java.nio.file.Path;
import java.util.List;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.repositories.IdentifierInRepositoryParameter;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.creedo.setup.ServerPaths;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.realkd.common.RuntimeBuilder;
import de.unibonn.realkd.common.parameter.DefaultParameter;
import de.unibonn.realkd.common.parameter.DefaultParameterContainer;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.ValueValidator;

/**
 *
 * @author bjacobs
 * 
 */
public class ContentPageBuilder implements PageBuilder, ParameterContainer,
		RuntimeBuilder<Page, CreedoSession> {

	private final DefaultParameterContainer parameterContainer = new DefaultParameterContainer();

	private final DefaultParameter<String> title;
	private final DefaultParameter<String> referenceName;
	private final Parameter<String> contentPageFileName;

	public ContentPageBuilder() {
		title = stringParameter("Title",
				"Title to be displayed above page content.", "",
				ValueValidator.ALWAYS_VALID_VALIDATOR, "");

		referenceName = stringParameter("Reference name",
				"Short name for links that reference page.", "",
				ValueValidator.ALWAYS_VALID_VALIDATOR, "");

		contentPageFileName = new IdentifierInRepositoryParameter<Path>(
				"Content file name",
				"The main content diplayed by the page",
				ApplicationRepositories.CONTENT_FOLDER_REPOSITORY,
				Repositories
						.getIdIsFilenameWithExtensionPredicate(Repositories.HTML_FILE_EXTENSIONS));
		// new
		// IdIsFilenameWithExtension<Path>(IdIsFilenameWithExtension.HTML_FILE_EXTENSIONS));

		parameterContainer.addParameter(title);
		parameterContainer.addParameter(referenceName);
		parameterContainer.addParameter(contentPageFileName);
	}

	@Override
	public Page build(CreedoSession session) {
		return new HtmlPage(title.getCurrentValue(),
				referenceName.getCurrentValue(),
				ServerPaths.getContentFileContentAsString(contentPageFileName
						.getCurrentValue()));
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return parameterContainer.getTopLevelParameters();
	}

}
