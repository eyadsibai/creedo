package de.unibonn.creedo.ui.core;

import javax.servlet.http.HttpSession;

import de.unibonn.creedo.ui.core.UiRegister.FrameCloser;
import de.unibonn.creedo.ui.core.UiRegister.IdGenerator;

/**
 * Interface of objects that represent configurations of frames that can be
 * materialized in the context of an http session.
 *
 * @author Björn Jacobs
 * 
 * @since 0.1.0
 * 
 * @version 0.1.0.1
 * 
 */
public interface FrameBuilder {

	public Frame build(IdGenerator idGenerator, FrameCloser closer, HttpSession session);

}
