package de.unibonn.creedo.ui.core;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.admin.options.Configurable;

/**
 * Default frame implementation that consists of composable components.
 * 
 * @author Mario Boley
 * 
 * @since 0.1.2
 * 
 * @version 0.1.2.1
 *
 */
public class DefaultFrame implements Frame, ActionProvider, Configurable<DefaultFrameOptions> {

	private final int id;

	private final List<UiComponent> components;

	private final String htmlBodyId;

	public DefaultFrame(int id, List<UiComponent> components, String htmlBodyId) {
		this.id = id;
		this.htmlBodyId = htmlBodyId;
		this.components = ImmutableList.copyOf(components);
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getView() {
		return "static/contentPageFrame";
	}

	@Override
	public Model getModel() {
		Model model = new BindingAwareModelMap();
		model.addAttribute("controllerName", htmlBodyId);
		model.addAttribute("frameId", id);
		model.addAttribute("applicationTitlePrefix", getOptions().titlePrefix());
		model.addAttribute("components", components);
		model.addAttribute("scriptFilenames", getAllScriptFilenames());

		for (UiComponent component : components) {
			model.addAllAttributes(component.getModel().asMap());
		}

		model.addAttribute("customStylesheet", getOptions().getCustomCssFileName().orElse(null));

		return model;
	}

	@Override
	public List<UiComponent> getComponents() {
		return components;
	}

	@Override
	public Collection<Integer> getActionIds() {
		Stream<ActionProvider> actionProviderStream = components.stream().filter(c -> c instanceof ActionProvider)
				.map(c -> (ActionProvider) c);
		return actionProviderStream.flatMap(p -> p.getActionIds().stream()).collect(Collectors.toSet());
	}

	@Override
	public ResponseEntity<String> performAction(int id, String... params) {
		for (UiComponent component : components) {
			if (component instanceof ActionProvider && ((ActionProvider) component).isActionAvailable(id)) {
				return ((ActionProvider) component).performAction(id);
			}
		}
		throw new IllegalArgumentException("No action with id " + id);
	}

	@Override
	public DefaultFrameOptions getDefaultOptions() {
		return new DefaultFrameOptions();
	}

	/**
	 * Override so that subclasses also use same option set by default.
	 * 
	 */
	@Override
	public String getOptionsSerializationId() {
		return DefaultFrame.class.getSimpleName();
	}

}
