package de.unibonn.creedo.ui.core;

import org.springframework.ui.Model;

/**
 * 
 * @author mboley
 * 
 */
public interface Page {

    public String getTitle();
    
    public String getReferenceName();

    public String getViewImport();

    public Model getModel();
    
//    public Action getActionById(String id);

}
