package de.unibonn.creedo.ui.core;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

import de.unibonn.creedo.Creedo;

@Controller
public class FrameController {

	private static final Logger LOGGER = Logger.getLogger(FrameController.class
			.getName());

	@Autowired
	private HttpSession session;

	@RequestMapping(value = "/showFrame.htm", method = RequestMethod.GET)
	public ModelAndView showFrame(@RequestParam(value = "frameId") int frameId) {

		UiComponent component = Creedo.getCreedoSession(session)
				.getUiComponent(frameId);

		if (!(component instanceof Frame)) {
			throw new IllegalArgumentException("No frame found with id "
					+ frameId);
		}

		return ((Frame) component).getModelAndView();
	}

	/**
	 * Handles client request for delayed or dynamic data loading.
	 * 
	 * @see {@link DataProvider}
	 * 
	 */
	@RequestMapping(value = "/getData.json", method = RequestMethod.GET)
	public @ResponseBody List<? extends Object> getData(
			@RequestParam(value = "creedoComponentId") int creedoComponentId,
			@RequestParam(value = "dataItemId") String dataItemId) {
		UiComponent component = Creedo.getCreedoSession(session)
				.getUiComponent(creedoComponentId);

		if (!(component instanceof DataProvider)) {
			throw new IllegalArgumentException(
					"No data provider found with id " + creedoComponentId);
		}

		DataProvider dataProvider = (DataProvider) component;

		if (!dataProvider.hasDataItem(dataItemId)) {
			throw new IllegalArgumentException("No data item with id "
					+ dataItemId);
		}

		return dataProvider.getDataItem(dataItemId);
	}

	/**
	 * This methods reads the parameter value for "parameters[]" directly from
	 * the HttpServletRequest because, for the case of "[","]", spring
	 * conversion produces "["",""]", i.e., an array with two empty strings.
	 */
	@RequestMapping(value = "/performAction.htm", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	@ResponseBody
	public ResponseEntity<String> performAction(HttpServletRequest request,
			@RequestParam(value = "actionProviderId") int actionProviderId,
			@RequestParam(value = "actionId") int actionId) {

		String[] paramValuesInRequest = request
				.getParameterValues("parameters[]");
		
		UiComponent component = Creedo.getCreedoSession(session)
				.getUiComponent(actionProviderId);
		if (!(component instanceof ActionProvider)) {
			throw new IllegalArgumentException(
					"Component does not provide actions.");
		}
		return ((ActionProvider) component).performAction(actionId,
				paramValuesInRequest);
	}
}
