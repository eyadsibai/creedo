package de.unibonn.creedo.ui;

import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.webapp.viewmodels.DeveloperViewModel;

public class IntrospectionPage implements Page {
	
	private static final String DEVELOPER_VIEW_MODEL_NAME = "viewModel";

	private final DeveloperViewModel developerViewModel;

	public IntrospectionPage(DeveloperViewModel developerViewModel) {
		this.developerViewModel = developerViewModel;
	}

	@Override
	public String getTitle() {
		return "Dashboard Introspection";
	}

	@Override
	public String getReferenceName() {
		return "Introspection";
	}

	@Override
	public String getViewImport() {
		return "introspectionPage.jsp";
	}

	@Override
	public Model getModel() {
		Model model=new BindingAwareModelMap();
		model.addAttribute(DEVELOPER_VIEW_MODEL_NAME, developerViewModel);
		return model;
	}

}
