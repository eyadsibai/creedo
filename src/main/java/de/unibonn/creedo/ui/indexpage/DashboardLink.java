package de.unibonn.creedo.ui.indexpage;

import de.unibonn.creedo.ui.core.FrameBuilder;

/**
 * Link for predefined analytics dashboards that can be opened from the index
 * page.
 * 
 * @author Björn Jacobs
 * @author Mario Boley
 * 
 * @see {@link DefaultIndexPage}
 * 
 * @since 0.1.0
 * 
 * @version 0.1.0.1
 * 
 */
public class DashboardLink {
	private final String title;
	private final String description;
	private final String imgPath;
	private final String imgCredits;
	private final FrameBuilder analyticsDashboardBuilder;

	public DashboardLink(String title, String description, String imgPath,
			String imgCredits,
			FrameBuilder analyticsDashboardBuilder) {
		this.title = title;
		this.description = description;
		this.imgPath = imgPath;
		this.imgCredits = imgCredits;
		this.analyticsDashboardBuilder = analyticsDashboardBuilder;
	}

	public String getTitle() {
		return title;
	}

	public String getDescription() {
		return description;
	}

	public String getImgPath() {
		return imgPath;
	}

	public String getImgCredits() {
		return imgCredits;
	}

	public FrameBuilder getAnalyticsDashboardBuilder() {
		return analyticsDashboardBuilder;
	}
}
