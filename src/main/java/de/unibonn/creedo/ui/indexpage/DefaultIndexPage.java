package de.unibonn.creedo.ui.indexpage;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import com.google.common.collect.ImmutableList;

import de.unibonn.creedo.admin.users.CreedoUser;
import de.unibonn.creedo.admin.users.Users;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.ActionProvider;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.creedo.webapp.DashboardController;
import de.unibonn.creedo.webapp.DemoProvider;
import de.unibonn.creedo.webapp.studies.StudyEngine;

/**
 * Page that displays some static content as well as links to all demos in db to
 * a logged in user (or a log-in invitation when user is not logged in).
 * 
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.1.2.1
 *
 */
public class DefaultIndexPage implements Page, ActionProvider {

	public static class OpenLinkAction implements Action {

		private final DashboardLink dashboardLink;

		private final int id;

		private final CreedoSession creedoSession;

		public DashboardLink getDashboardLink() {
			return dashboardLink;
		}

		private OpenLinkAction(CreedoSession session, int actionId,
				DashboardLink link) {
			this.dashboardLink = link;
			this.id = actionId;
			this.creedoSession = session;
		}

		@Override
		public String getReferenceName() {
			return dashboardLink.getTitle();
		}

		@Override
		public ResponseEntity<String> activate(String... params) {
			if (creedoSession.analyticsDashboardOpen()) {
				return new ResponseEntity<String>(
						"Another analytics dashboard already open. Please close first.",
						HttpStatus.FORBIDDEN);
			}
			creedoSession.createMiningDashboard(dashboardLink
					.getAnalyticsDashboardBuilder());
			return new ResponseEntity<String>(
					DashboardController.GET_DASHBOARD_URL, HttpStatus.OK);
		}

		@Override
		public ClientWindowEffect getEffect() {
			return ClientWindowEffect.POPUP;
		}

		@Override
		public int getId() {
			return id;
		}

	}

	public class DashboardLinkSection {

		private final String title;
		private final List<OpenLinkAction> actions;

		public DashboardLinkSection(String title, List<OpenLinkAction> actions) {
			this.title = title;
			this.actions = actions;
		}

		public String getTitle() {
			return title;
		}

		public List<OpenLinkAction> getActions() {
			return actions;
		}

	}

	private static final List<DashboardLinkProvider> DASHBOARD_PROVIDER = ImmutableList
			.of(StudyEngine.INSTANCE, DemoProvider.INSTANCE);

	private final String title;

	private final Model model;

	private final String referenceName;

	private final Map<Integer, OpenLinkAction> actionIdToOpenLinkAction;

	private final CreedoSession session;

	private final String loginInviteHtml;

	public DefaultIndexPage(CreedoSession session, String title,
			String referenceName, String contentPageContent, String loginInvite) {
		this.session = session;
		this.actionIdToOpenLinkAction = new HashMap<>();
		this.model = new BindingAwareModelMap();
		this.title = title;
		this.referenceName = referenceName;
		this.model.addAttribute("mainContent", contentPageContent);
		this.loginInviteHtml = loginInvite;
	}

	@Override
	public String getTitle() {
		return title;
	}

	@Override
	public String getViewImport() {
		return "indexPage.jsp";
	}

	@Override
	public Model getModel() {
		CreedoUser user = session.getUser();
		if (!user.equals(Users.DEFAULT_USER)) {
			List<DashboardLinkSection> linkSections = new ArrayList<>();

			DASHBOARD_PROVIDER.forEach(provider -> {
				DashboardLinkSection section = compileSection(user, provider);
				if (section.actions.size() > 0) {
					linkSections.add(section);
				}
			});

			this.model.addAttribute("actionSections", linkSections);
		} else {
			model.addAttribute("loginInvitationHtml", this.loginInviteHtml);
		}
		return model;
	}

	private DashboardLinkSection compileSection(CreedoUser user,
			DashboardLinkProvider linkProvider) {
		List<DashboardLink> dashboardLinks = new ArrayList<>();
		dashboardLinks.addAll(linkProvider.getDashboardLinks(user));

		List<OpenLinkAction> dashboardLinkLinks = new ArrayList<>();

		for (DashboardLink link : dashboardLinks) {
			Integer actionId = session.getUiRegister().getIdGenerator()
					.getNextId();
			OpenLinkAction openLinkAction = new OpenLinkAction(session,
					actionId, link);
			dashboardLinkLinks.add(openLinkAction);
			actionIdToOpenLinkAction.put(actionId, openLinkAction);
		}
		DashboardLinkSection linkSection = new DashboardLinkSection(
				linkProvider.getTitle(), dashboardLinkLinks);
		return linkSection;
	}

	@Override
	public String getReferenceName() {
		return referenceName;
	}

	@Override
	public Collection<Integer> getActionIds() {
		return actionIdToOpenLinkAction.keySet();
	}

	@Override
	public ResponseEntity<String> performAction(int id, String... params) {
		OpenLinkAction openLinkAction = actionIdToOpenLinkAction.get(id);
		if (openLinkAction == null) {
			throw new IllegalArgumentException("No action with id " + id);
		}
		return openLinkAction.activate(params);
	}

	@Override
	public boolean isActionAvailable(int id) {
		return actionIdToOpenLinkAction.containsKey(id);
	}

}
