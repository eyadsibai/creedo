package de.unibonn.creedo.ui.signup;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.Optional;
import java.util.logging.Logger;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import de.unibonn.creedo.WebConstants;
import de.unibonn.creedo.admin.users.CreedoUser;
import de.unibonn.creedo.admin.users.Users;
import de.unibonn.creedo.common.BCrypt;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.webapp.CreedoSession;

/**
 * Action that, on success, terminates a linked Creedo session and binds a new
 * Creedo session for specified user instead to the http session object that was
 * referred to by the old Creedo session.
 * 
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.3.0
 *
 */
public class LoginAction implements Action {

	private static final Logger LOGGER = Logger.getLogger(LoginAction.class
			.getName());

	private static final String INACTIVE_USER_MSG = "The account has not yet been activated by the administrator.";

	private static final String UNKNOWN_USER_MSG = "There is no such user in the database.";

	private static final String WRONG_PW_MSG = "There is no such user/password combination in the database. Wrong password?";

	private final CreedoSession session;

	private final int id;

	/**
	 * 
	 * @param id
	 * @param session
	 *            the session to be terminated by this action if successful
	 */
	public LoginAction(int id, CreedoSession session) {
		this.session = session;
		this.id = id;
	}

	@Override
	public String getReferenceName() {
		return "Login";
	}

	@Override
	public ResponseEntity<String> activate(String... params) {
		checkArgument(params.length == 2,
				"Expecting exactly two arguments for login action: email and password");

		String userName = params[0];
		String password = params[1];
		Optional<CreedoUser> user = Users.getUser(userName);

		// No such user in database
		if (!user.isPresent()) {
			return new ResponseEntity<String>(UNKNOWN_USER_MSG,
					HttpStatus.EXPECTATION_FAILED);
		}

		 // User not active
		 if (!user.get().active()) {
		 return new ResponseEntity<String>(INACTIVE_USER_MSG,
		 HttpStatus.EXPECTATION_FAILED);
		 }

		// Wrong password
		if (!BCrypt.checkpw(password, user.get().hashedPassword())) {
			return new ResponseEntity<String>(WRONG_PW_MSG,
					HttpStatus.EXPECTATION_FAILED);
		}

		session.getHttpSession().invalidate();
		LOGGER.info("Login of user '" + user.get().id() + "'");
		CreedoSession.createNewCreedoSession(user.get(),
				session.getHttpSession());

		return new ResponseEntity<String>(WebConstants.APPLICATION_ROOT
				+ WebConstants.HOME_PATH, HttpStatus.OK);
	}

	@Override
	public ClientWindowEffect getEffect() {
		return ClientWindowEffect.REDIRECT;
	}

	@Override
	public int getId() {
		return id;
	}

}
