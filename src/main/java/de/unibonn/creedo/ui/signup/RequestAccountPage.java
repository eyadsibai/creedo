package de.unibonn.creedo.ui.signup;

import static com.google.common.base.Preconditions.checkArgument;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.validation.support.BindingAwareModelMap;

import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.ActionProvider;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.ui.core.UiComponent;

/**
 * Page that wraps an action for registering new accounts and provides visual
 * control elements for a user to access this action.
 * 
 * @author mboley
 *
 */
public class RequestAccountPage implements Page, UiComponent, ActionProvider {

	private final int id;

	private final String note;

	private final RequestAccountAction requestAccountAction;

	public RequestAccountPage(int id, String note,
			RequestAccountAction requestAccountAction) {
		checkArgument(requestAccountAction != null,
				"Must receive a non-null request account action");
		this.id = id;
		this.note = note;
		this.requestAccountAction = requestAccountAction; // = new
															// RequestAccountAction();
	}

	@Override
	public String getTitle() {
		return "Request new account";
	}

	@Override
	public String getReferenceName() {
		return "Request Account";
	}

	@Override
	public String getViewImport() {
		return "requestAccountPage.jsp";
	}

	@Override
	public Model getModel() {
		Model model = new BindingAwareModelMap();
		model.addAttribute("accountRequestNoteHtml", note);
		model.addAttribute("requestAccountPageComponentId", getId());
		model.addAttribute("requestAccountActionId",
				requestAccountAction.getId());
		return model;
	}

	@Override
	public Collection<Integer> getActionIds() {
		return Arrays.asList(requestAccountAction.getId());
	}

	@Override
	public ResponseEntity<String> performAction(int id, String... params) {
		checkArgument(requestAccountAction.getId()==id,
				"unknown action %s", id);

		ResponseEntity<String> result = requestAccountAction.activate(
				params[0], params[1]);

		return result;
	}

	/**
	 * Allows to chain in an action that is triggered on a successful
	 * registration (that would send HttpStatus.OK). Provided action then takes
	 * over control and is responsible for providing final HttpResponse.
	 * 
	 * Action is assumed to require no parameters and to have return client
	 * window effect "refresh".
	 * 
	 * Will be replaced by general action composition framework or observer
	 * pattern in the future.
	 * 
	 */
	public void addRegistrationSuccessAction(Action action) {
		this.requestAccountAction.setOnSuccessAction(action);
	}

	@Override
	public boolean isActionAvailable(int id) {
		return requestAccountAction.getId()==id;
	}

	@Override
	public int getId() {
		return id;
	}

	@Override
	public String getView() {
		return getViewImport();
	}

	@Override
	public List<UiComponent> getComponents() {
		return Arrays.asList();
	}

}
