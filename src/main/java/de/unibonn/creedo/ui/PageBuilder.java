package de.unibonn.creedo.ui;

import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.webapp.CreedoSession;

/**
 *
 * @author bjacobs
 */
public interface PageBuilder {

	public Page build(CreedoSession session);

}
