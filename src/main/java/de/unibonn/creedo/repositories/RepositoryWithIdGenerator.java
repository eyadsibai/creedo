package de.unibonn.creedo.repositories;

public interface RepositoryWithIdGenerator<K, T> extends Repository<K, T> {

	public K addContentAndGenerateId(T content);

}
