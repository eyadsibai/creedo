package de.unibonn.creedo.boot;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import de.unibonn.creedo.ApplicationRepositories;
import de.unibonn.creedo.admin.users.DefaultUserGroup;
import de.unibonn.creedo.admin.users.Users;
import de.unibonn.creedo.repositories.Repositories;
import de.unibonn.creedo.repositories.RepositoryEntry;
import de.unibonn.creedo.ui.PageBuilder;
import de.unibonn.creedo.ui.core.Action;
import de.unibonn.creedo.ui.core.DefaultPageFrame;
import de.unibonn.creedo.ui.core.Frame;
import de.unibonn.creedo.ui.core.LoadPageAction;
import de.unibonn.creedo.ui.core.Page;
import de.unibonn.creedo.ui.core.PageContainer;
import de.unibonn.creedo.ui.core.PageFrame;
import de.unibonn.creedo.ui.signup.DefaultLoginPage;
import de.unibonn.creedo.ui.signup.LoginAction;
import de.unibonn.creedo.ui.signup.RegistrationRequestConfirmationPage;
import de.unibonn.creedo.ui.signup.RequestAccountPage;
import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.realkd.common.RuntimeBuilder;
import de.unibonn.realkd.common.parameter.DefaultParameterContainer;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.SubCollectionParameter;

/**
 * <p>
 * Creates and configures a frame object with all features necessary for the
 * Creedo mainframe.
 * </p>
 * 
 * <p>
 * WARNING: As of now this class is referenced directly in a db-based
 * repository. So make sure to update init scripts when moving or renaming this
 * class as well when you change its declared parameters.
 * </p>
 * 
 * @author Mario Boley
 * 
 * @since 0.1.0
 * 
 * @version 0.1.0.1
 *
 */
public class DefaultMainFrameBuilder implements ParameterContainer,
		RuntimeBuilder<PageFrame, CreedoSession> {

	// private static final Logger LOGGER = Logger
	// .getLogger(DefaultMainFrameBuilder.class.getName());

	private final DefaultParameterContainer defaultParameterContainer;

	private final SubCollectionParameter<String, List<String>> optionalNavbarContents;

	private final SubCollectionParameter<String, List<String>> optionalFooterContents;

	public DefaultMainFrameBuilder() {
		this.defaultParameterContainer = new DefaultParameterContainer();
		this.optionalNavbarContents = Repositories
				.getIdentifierListOfRepositoryParameter(
						"Navbar entries",
						"List of optional content to be linked from the navbar of frame.",
						ApplicationRepositories.PAGE_REPOSITORY);
		this.optionalFooterContents = Repositories
				.getIdentifierListOfRepositoryParameter(
						"Footer entries",
						"List of optional content to be linked from the footer of frame.",
						ApplicationRepositories.PAGE_REPOSITORY);
		this.defaultParameterContainer.addParameter(optionalNavbarContents);
		this.defaultParameterContainer.addParameter(optionalFooterContents);
	}

	public PageFrame build(final CreedoSession creedoSession) {
		PageContainer pageContainer = new PageContainer(creedoSession
				.getUiRegister().getNextId());

		RequestAccountPage requestAccountPage = (RequestAccountPage) ApplicationRepositories.PAGE_REPOSITORY
				.getEntry("signuppage").getContent().build(creedoSession);

		creedoSession.getUiRegister().registerUiComponent(requestAccountPage);
		Action loadAccountRequestPage = new LoadPageAction(creedoSession
				.getUiRegister().getNextId(), requestAccountPage, pageContainer);

		Page accountRequestConfirmationPage = new RegistrationRequestConfirmationPage();
		requestAccountPage.addRegistrationSuccessAction(new LoadPageAction(
				creedoSession.getUiRegister().getNextId(),
				accountRequestConfirmationPage, pageContainer));

		Action loginAction = new LoginAction(creedoSession.getUiRegister()
				.getNextId(), creedoSession);

		DefaultLoginPage loginPage = new DefaultLoginPage(creedoSession
				.getUiRegister().getNextId(), loginAction,
				loadAccountRequestPage);
		creedoSession.getUiRegister().registerUiComponent(loginPage);

		Page indexPage = ((RuntimeBuilder<Page, CreedoSession>) ApplicationRepositories.PAGE_REPOSITORY
				.getEntry("indexpage").getContent()).build(creedoSession);

		List<Action> navbarActions = new ArrayList<>();
		List<Action> footerActions = new ArrayList<>();
		// String loadIndexPageActionId = "loadIndexPage";
		navbarActions.add(new LoadPageAction(creedoSession.getUiRegister()
				.getNextId(), indexPage, pageContainer));

		if (creedoSession.getUser().groups()
				.contains(DefaultUserGroup.ADMINISTRATOR)) {

			navbarActions.add(new Action() {

				private final int id = creedoSession.getUiRegister()
						.getNextId();

				@Override
				public String getReferenceName() {
					return "Administration";
				}

				@Override
				public ResponseEntity<String> activate(String... params) {
					Frame adminFrame = new AdminFrameBuilder()
							.build(creedoSession);

					return new ResponseEntity<String>("showFrame.htm?frameId="
							+ adminFrame.getId(), HttpStatus.OK);
				}

				@Override
				public ClientWindowEffect getEffect() {
					return ClientWindowEffect.POPUP;
				}

				@Override
				public int getId() {
					return id;
				}
			});
		}

		if (creedoSession.getUser().groups()
				.contains(DefaultUserGroup.DEVELOPER)) {
			navbarActions.add(new LoadPageAction(creedoSession.getUiRegister()
					.getNextId(), creedoSession
					.getCustomDashboardCreationPage(), pageContainer));

			// navbarActions.add(new
			// LoadPageAction(creedoSession.getUiRegister()
			// .getNextId(), creedoSession.getDatabaseUploadPage(),
			// pageContainer));
		}

		for (String contentId : optionalNavbarContents.getCurrentValue()) {
			RepositoryEntry<String, RuntimeBuilder<Page, CreedoSession>> entry = ApplicationRepositories.PAGE_REPOSITORY
					.getEntry(contentId);
			// PageBuilder pageBuilder = pagesDAO.getPageBuilder(pageId);
			PageBuilder pageBuilder = (PageBuilder) entry.getContent();
			Page page = pageBuilder.build(creedoSession);

			navbarActions.add(new LoadPageAction(creedoSession.getUiRegister()
					.getNextId(), page, pageContainer));

		}
		for (String contentId : optionalFooterContents.getCurrentValue()) {
			RepositoryEntry<String, RuntimeBuilder<Page, CreedoSession>> entry = ApplicationRepositories.PAGE_REPOSITORY
					.getEntry(contentId);
			PageBuilder pageBuilder = (PageBuilder) entry.getContent();
			Page page = pageBuilder.build(creedoSession);
			footerActions.add(new LoadPageAction(creedoSession.getUiRegister()
					.getNextId(), page, pageContainer));
		}

		if (creedoSession.getUser() == Users.DEFAULT_USER) {
			navbarActions.add(new LoadPageAction(creedoSession
					.getLoadLoginPageActionId(), loginPage, pageContainer));
		} else {
			Action logOutAction = new Action() {

				private final int id = creedoSession.getUiRegister()
						.getNextId();

				@Override
				public String getReferenceName() {
					return "Logout";
				}

				@Override
				public ResponseEntity<String> activate(String... params) {
					creedoSession.tearDownDashboard();
					// Object timeoutHandlerObj = creedoSession.getHttpSession()
					// .getAttribute(WebConstants.TIMEOUT_HANDLER_KEY);
					// if (timeoutHandlerObj instanceof TimeoutHandler) {
					// ((TimeoutHandler) timeoutHandlerObj).stop();
					// LOGGER.info("Stopped TimeoutHandler.");
					// }
					creedoSession.getHttpSession().invalidate();
					return new ResponseEntity<String>(HttpStatus.OK);
					// createNewCreedoSession(User.DEFAULT_USER, httpSession);
				}

				@Override
				public ClientWindowEffect getEffect() {
					return ClientWindowEffect.REFRESH;
				}

				@Override
				public int getId() {
					return id;
				}

			};
			navbarActions.add(logOutAction);
		}
		DefaultPageFrame result = creedoSession
				.getUiRegister()
				.createDefaultFrame(pageContainer, navbarActions, footerActions);
		// result.performAction(loadIndexPageActionId);

		return result;
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return this.defaultParameterContainer.getTopLevelParameters();
	}

}
