package de.unibonn.creedo.boot;

import static de.unibonn.realkd.common.parameter.Parameters.stringParameter;

import java.util.List;

import de.unibonn.creedo.ui.signup.RequestAccountAction;
import de.unibonn.creedo.ui.signup.RequestAccountPage;
import de.unibonn.creedo.webapp.CreedoSession;
import de.unibonn.realkd.common.RuntimeBuilder;
import de.unibonn.realkd.common.parameter.DefaultParameterContainer;
import de.unibonn.realkd.common.parameter.Parameter;
import de.unibonn.realkd.common.parameter.ParameterContainer;
import de.unibonn.realkd.common.parameter.ValueValidator;

public class DefaultAccountRequestPageBuilder implements
		RuntimeBuilder<RequestAccountPage, CreedoSession>, ParameterContainer {

	private Parameter<String> noteParameter;
	private final DefaultParameterContainer parameterContainer;

	public DefaultAccountRequestPageBuilder() {
		this.parameterContainer = new DefaultParameterContainer();
		this.noteParameter = stringParameter(
				"Account request note",
				"A note that is displayed by page together with the sign up form",
				"", ValueValidator.VALUE_NOT_NULL_VALIDATOR, "");
		this.parameterContainer.addParameter(noteParameter);
	}

	@Override
	public RequestAccountPage build(CreedoSession context) {
		RequestAccountAction requestAccountAction = new RequestAccountAction(
				context.getUiRegister().getNextId());
		return new RequestAccountPage(context.getUiRegister().getNextId(),
				noteParameter.getCurrentValue(), requestAccountAction);
	}

	@Override
	public List<Parameter<?>> getTopLevelParameters() {
		return this.parameterContainer.getTopLevelParameters();
	}

}
